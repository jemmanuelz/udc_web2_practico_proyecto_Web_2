<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Publicacion_Destacada_Servicio extends Model
{
    

    protected $table = 'publicaciones_destacadas_servicio';
  
   	protected $fillable = [

  						              'id',
                            'fecha',
                            'activo',
                            'descripcion',
                            'profesional_id',
                            'servicio_id'
                           ];

  	protected $guarded = ['id'];
 
    
    public function servicio()
    {
      return $this->belongsTo('App\Servicio');
    }

    public function profesional()
    {
      return $this->belongsTo('App\Profesional');
    }

}

