<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

use App\Servicio;
use App\Comentario;
use Carbon\Carbon;
use Auth;
use App\Estado_Contratacion as Estado;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nick', 'email', 'password',
    ];

   
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function profesional()
    {
      return $this->hasOne('App\Profesional','user_id')->where('activo',true);
    }

    public function contrataciones_servicios()
    {
      return $this->hasMany('App\Contratacion_Servicio');
    }

    public function comentarios()
    {
      return $this->hasMany('App\Cometario');
    }

    public function perfil_usuario()
    {
      return $this->hasOne('App\Perfil_Usuario')->where('activo',true);
    }



    public function comentarios_de_hoy()
    {

     if (Auth::check()) {

        if(! (Auth::user()->profesional()->get()->isEmpty() ) ){


          $profesiones= Auth::user()->profesional->profesion->lists('id');
          $servicios= Servicio::whereIn('profesion_id',$profesiones->toArray())->get();
          $comentarios=Comentario::whereIn('servicio_id',$servicios->lists('id'))->whereDate('fecha','=',Carbon::now()->format('Y-m-d'))->orderBy('fecha','desc')->get();

          if($comentarios->isEmpty()){
            return null;
          }
              return $comentarios;
          

        }

      }
      

     return null; 
    }




    public function mostrar_comentarios()
    {


        $comentarios= Auth::user()->comentarios_de_hoy();

        if(empty($comentarios)){
            echo '<li> <a><i class="fa fa-users text-aqua"></i> no hay comentarios </a></li>';
           return null; 
        }

        for ($i=0; $i<6 ; $i++) { 
                
            if(!empty($comentarios->get($i)) ){

                echo '<li> <a><i class="fa fa-users text-aqua"></i>servicio :'.$comentarios->get($i)->servicio->profesion->tipo_profesion->nombre.'descripcion :'.$comentarios->get($i)->descripcion ."</a></li>";                                 

            }


        }

    
      return null; 

    }


    static function buscar($usuario)
    {
        return User::find($usuario);
    }


    public function todas_contrataciones_pendientes()
    {

      if (Auth::check()) {


        if(! (Auth::user()->profesional()->get()->isEmpty() ) ){

          $profesiones= Auth::user()->profesional->profesion()->get();

          if(empty($profesiones)){
              return null;
          }

          $servicios= Servicio::whereIn('profesion_id',$profesiones->lists('id')->toArray())->get()->lists('id');


         // $contrataciones_pendientes=Contratacion_Servicio::pendientes();

        
          $contrataciones=Contratacion_Servicio::whereIn('servicio_id',$servicios->toArray())->where('estado_contratacion_id',Estado::pendiente())->orderBy('fecha','desc')->get();

         
          
          return $contrataciones;
        }
     }

     return null; 
        
    }



     public function mostrar_contrataciones()
    {


        $contrataciones= Auth::user()->todas_contrataciones_pendientes();

        if(empty($contrataciones)){
            echo '<li> <a><h3>no hay contrataciones pendientes </h3></a></li>';
            return null; 
        }

        for ($i=0; $i<6 ; $i++) { 
                
            if(!  empty($contrataciones->get($i)) ){

                echo '<li> <a><h3>servicio :'.$contrataciones->get($i)->servicio->profesion->tipo_profesion->nombre.'  ofertante :'.$contrataciones->get($i)->usuario->nick."</h3> </a></li>";

            }


        }

      return null;

    }



    public function crear_perfil($datos)
    {
        $this->perfil_usuario()->create($datos->all());
    }



    public function actualizar_perfil($datos)
    {
        $this->perfil_usuario()->update($datos->only('nombre_completo', 'telefono','domicilio' ));
    }




    public function crear_profesional($datos)
    {
        $this->profesional()->create($datos->all());
    }



    public function actualizar_profesional($datos)
    {
        $this->profesional()->update($datos->only('cuil','telefono_alternativo'));
    }


    public function scopeBaja()
    {
      $this->activo=false;
      $this->save();
    }




}
