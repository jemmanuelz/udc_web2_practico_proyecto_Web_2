<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoria_Servicio extends Model
{
   	protected $table = 'categorias_servicios';
  
   	protected $fillable = [

  						              'id',
                            'valor',
                            'activo',

                           ];

  	protected $guarded = ['id'];

  	public function sub_categorias()
    {
    	return $this->hasMany('App\Sub_Categoria_Servicio','categoria_servicio_id');
    } 


    public function scopeDeterminada($query,$value)
    {
      return $query->where('valor','LIKE',$value)->get()->first()->id;
    }


    public function scopeMostrarNav($query)
    {
      return $query->where('valor','NOT LIKE','Cotizable');
    }


    public function getValorAttribute($value){
      
      $value=ucwords( $value );
      
      return $value;
        
    }
}

