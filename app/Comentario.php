<?php

namespace App;

use Carbon\Carbon as Carbon;
use Illuminate\Database\Eloquent\Model;

class Comentario extends Model
{
    protected $table = 'comentarios';
  
   	protected $fillable = [

  						              'id',
                            'fecha',
                            'descripcion',
                            'user_id',
                            'servicio_id',
                            
                           ];

  	protected $guarded = ['id'];

  	public function servicio()
    {
      return $this->belongsTo('App\Servicio');
    }

    public function usuario()
    {
      return $this->belongsTo('App\User','user_id');
    }


     public function getFechaAttribute($value)
    {
      return Carbon::parse($value)->format('d-m-Y H:i:s');
    }


    
    static function crear($user,$servicio,$descripcion)
    {
       Comentario::create(['user_id' => $user,'servicio_id' => $servicio, 'descripcion' => $descripcion,'fecha' => Carbon::now()->format('Y-m-d H:i:s') ]);
    
    }


}

