<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estado_Contratacion extends Model
{
    protected $table = 'estados_contrataciones';
  
   	protected $fillable = [

  						    'id',
          					'valor'
  						   
                         ];


    public function contrataciones_servicios()
    {
      return $this->hasMany('App\Contratacion_Servicio','id','estado_contratacion_id');
    } 


    public function scopePendiente($query)
    {
    	return $query->where('valor','LIKE','%pendiente%')->get()->first()->id; 
    }

    public function scopeCancelada($query)
    {
    	return $query->where('valor','LIKE','%cancelada%')->get()->first()->id; 
    }

    public function scopeAceptada($query)
    {
    	return $query->where('valor','LIKE','%aceptada%')->get()->first()->id; 
    }


    public function scopeListado($query)
    {
      return $query->get()->lists('valor','id'); 
    }


}
