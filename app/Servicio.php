<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon as Carbon;
class Servicio extends Model
{
    
    protected $table = 'servicios';
  
   	protected $fillable = [

  						              'id',
                            'es_contizado',
                            'nombre',
                            'precio',
                            'activo',
                            'descripcion',
                            'ubicacion_servicio_id',
                            'tipo_servicio_id',
                            'profesion_id',
                            'like'
                           
                           ];

  	protected $guarded = ['id'];


   

    public function ubicacion_servicio()
    {
      return $this->belongsTo('App\Ubicacion_Servicio');
    }

    public function tipo_servicio()
    {
      return $this->belongsTo('App\Tipo_Servicio');
    }
     public function profesion()
    {
      return $this->belongsTo('App\Profesion');
    }

    public function publicaciones_destacadas_profesional()
    {
      return $this->hasMany('App\Publicacion_Destacada_Servicio');
    } 

    public function comentarios()
    {
      return $this->hasMany('App\Comentario');
    } 

    public function contrataciones_servicios()
    {
      return $this->hasMany('App\Contratacion_Servicio');
    } 

//created_at
    public function getCreatedAtAttribute($value)
    {
      return Carbon::parse($value)->format('d-m-Y H:i:s');
    }
    

    public function comentarios_ordenados()
    {
      return $this->comentarios()->orderBy('fecha','desc')->get();
    }





    public function dar_like()
    {
       
        $this->like+=1;
        $this->save();
    }


     public function dar_dislike()
    {
       
        $this->dislike+=1;
        $this->save();
    }


    public function traer_usuario()
    {
      return $this->profesion->profesional->usuario;
    }



    public function verificar_auto_contratacion()
    {
      if (\Auth::check() ){
        if(\Auth::user()->id != $this->profesion->profesional->usuario->id){
          return true;
        }

      }
       
          return false;
     




    }



}
