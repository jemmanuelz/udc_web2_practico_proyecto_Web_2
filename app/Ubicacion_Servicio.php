<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ubicacion_Servicio extends Model
{
    protected $table = 'ubicacion_servicios';
  
   	protected $fillable = [

  						              'id',
                           	'ciudad'
                           
                           ];

  	protected $guarded = ['id'];
  	

  	public function servicio()
    {
      return $this->hasMany('App\Servicio');
    }

}
