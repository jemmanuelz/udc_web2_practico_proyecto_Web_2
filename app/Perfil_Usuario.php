<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Perfil_Usuario extends Model
{
    

    protected $table = 'perfiles_usuarios';

   	protected $fillable = [

  						              'id',
  						              'nombre_completo',
                            'telefono',
                            'user_id',
                            'domicilio'
                            
                           ];

  	protected $guarded = ['id'];


    public function usuario()
    {
      return $this->belongsTo('App\User');
    }

     public function setTelefonoAttribute($value)
    {
      //saca el formato para que se normalize los datos en la BD
      $value=trim($value,"_");
      $value=str_replace(")","",$value);
      $value=str_replace("(","",$value);
      $value=str_replace("+54","",$value);

      $this->attributes['telefono'] = str_replace("-","",$value);

    }


    public function scopeBaja()
    {
      $this->activo=false;
      $this->save();
    }


}
