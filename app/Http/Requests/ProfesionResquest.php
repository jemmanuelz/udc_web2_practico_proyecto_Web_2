<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProfesionResquest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'experiencia'         => 'required|numeric|digits_between:1,3',
            'tipo_profesion_id'   => 'required|exists:tipo_profesiones,id'
            ];
    }


    public function messages()
    {
        return [
              'tipo_profesion_id.required'   => 'asignarle un tipo a la  profesion es necesario',
              'tipo_profesion_id.exists'   => 'el tipo de profesion ingresada no esta registrada'
            ];
    }
}
