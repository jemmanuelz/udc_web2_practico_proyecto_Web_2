<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PerfilUsuarioResquest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre_completo'      => 'required|regex: [^.*(?=.*[a-zA-ZñÑ\t\s]).*$]|between:3,50',  
            'telefono'    => 'regex:[[\+][5][4][\(]\d{3}[\)]\d{3}[\-]\d{4}]',
            'domicilio'   => 'required|regex: [^.*(?=.*[0-9])(?=.*[a-zA-ZñÑ\s]).*$]',
        ];
    }



    public function messages()
    {
        return[
        
            'nombre_completo.required' => 'el nombre completo del perfil en el usuario es necesario',
            'nombre_completo.regex' => 'en el nombre completo solo se aceptan letras',

        ];
        

    }


}
