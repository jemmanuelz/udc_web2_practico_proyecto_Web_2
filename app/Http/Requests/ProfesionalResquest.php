<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Profesional;

class ProfesionalResquest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }















    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        
     \Validator::extend('valid_cuil', function($attribute, $value, $parameters)
        {
          
            
            $value=trim($value,"_");
            $value= str_replace("-","",$value);  

            //busca el con el id que le pasa en caso de update

            $profesional=Profesional::find($this->profesional);

            //si devuelve un objeto(si la variable no esta vacia)
            if(!empty($profesional)){
                //se fija si el dni que se manda es igual al que esta guardado en la base de datos. Si es igual es porque no lo cambio y devuelve true
                if ($profesional->cuil == $value){
                    return true;
                }
            }
            //en caso de que la variable del create, se realiza la busqueda de Cooperativas por su dni
            $prof=profesional::all()->lists('cuil');
            //se convierte la coleccion $coop en Array para poder usarlo en lla funcion in_array
            //si da verdadero es porque el valor dni ingresado le pertenece a otra cooperativista
           if(in_array($value, $prof->toArray()))
            {

                return false;
            }else{
                //si no no. y esta habilitado para ser usado
                return true;
            }
         
           
           
    }); 











        
        return [

            'cuil'                  => 'required|regex: [\d{2}[\-]\d{8}[\-]\d{1}]|valid_cuil',
            'telefono_alternativo'  => 'regex:[[0][\+][\(]\d{3}[\)][4][\-]\d{6}]',

        ];
    }




     public function messages()
    {
        return[
        
            'telefono_alternativo.regex' => 'formato para el telefono altenativo es invalido',
            'cuil.valid_cuil' => 'el cuil esta en uso',
           
        ];
        

    }







}
