<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ServicioResquest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }




   

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      \Validator::extend('valid_precio', function($attribute, $value, $parameters)
      {
          if ($value < 0) {
              return false;
          }
            
          return true;
           
    });

        return [
            
            'nombre'      => 'regex: [^.*(?=.*[a-zA-ZñÑ\t\s]).*$]|between:3,50',  
            'precio'       => 'numeric|digits_between:1,8|valid_precio',
            'descripcion'  => 'string|max:200',
            'ubicacion_servicio_id' => 'required|exists:ubicacion_servicios,id',
            'tipo_servicio_id'     => 'required|exists:tipos_servicios,id',
            'profesion_id'         => 'exists:profesiones,id'
        ];
    }



     public function messages()
    {
        return [
              'precio.valid_precio' => 'el precio debe ser una cifra de valor positivo',
              'tipo_servicio_id.required'   => 'asignarle un tipo al servicio es necesario',
              'tipo_servicio_id.exists'   => 'el tipo de servicio ingresado no esta registrado',
              'ubicacion_servicio_id.required'   => 'asignarle una forma de ubicacion es necesario',
              'ubicacion_servicio_id.exists'   => 'el tipo de ubicacion ingresado no esta registrado',
              'profesion_id.required'   => 'asignarle una profesion al servicio es necesario',
              'profesion_id.exists'   => 'el tipo profesion ingresada no esta registrada'
            ];
    }


}
