<?php
use App\Categoria_Servicio as Categoria;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



Route::get('/', function () {

	$categorias=Categoria::mostrarNav()->get();

    return view('welcome',compact('categorias'));

});

Route::get('usuario/datos','UsuarioController@datosUsuarios')
															->name('usuario.datos');

Route::get('usuario/datos/edit','UsuarioController@datosUsuariosEdit')
																->name('usuario.datos.edit');

Route::put('usuario//datos/edit','UsuarioController@datosUsuariosUpdate')
																->name('usuario.datos.update');															

Route::resource('usuario','UsuarioController');

Route::resource('usuario/perfil','PerfilUsuarioController',['names' => 
																		[
																		 'create'  => 'usuario.perfil.create',
																		 'edit'    => 'usuario.perfil.edit',
																		 'update'  => 'usuario.perfil.update',
																		 'show'    => 'usuario.perfil.show',
																		 'store'   => 'usuario.perfil.store',
																		 'destroy' => 'usuario.perfil.destroy'
																		],
																		'except' => ['index']

																	  ]);

Route::resource('usuario/profesional','ProfesionalController',['names' => 
																		[
																		 'create'  => 'usuario.profesional.create',
																		 'edit'    => 'usuario.profesional.edit',
																		 'update'  => 'usuario.profesional.update',
																		 'show'    => 'usuario.profesional.show',
																		 'store'   => 'usuario.profesional.store',
																		 'destroy' => 'usuario.profesional.destroy'
																		],
																		'except' => ['index']

																	  ]);

Route::resource('profesional/profesion','ProfesionController',['names' => 
																		[
																		 'create'  => 'profesional.profesion.create',
																		 'edit'    => 'profesional.profesion.edit',
																		 'update'  => 'profesional.profesion.update',
																		 'show'    => 'profesional.profesion.show',
																		 'store'   => 'profesional.profesion.store',
																		 'destroy' => 'profesional.profesion.destroy',
																		 'index'   => 'profesional.profesion.index'
																		],
																	
																	  ]);

Route::get('profesional/profesion/{profesion}/servicio/create','ServicioController@ServicioProfesionDeterminada')->name('profesion.servicio.create');
//Route::post('profesional/{profesional}/profesion/{profesion}/servicio','ServicioController@store')->name('profesion.servicio.store');

Route::resource('profesional/servicio','ServicioController',['names' => 
																		[
																		 'create'  => 'servicio.create',
																		 'edit'    => 'servicio.edit',
																		 'update'  => 'servicio.update',
																		 'show'    => 'servicio.show',
																		 'store'   => 'servicio.store',
																		 'destroy' => 'servicio.destroy',
																		 'index'   => 'servicio.index'
																		],
																	
																	  ]);





Route::get('ajax-sub-categoria/{sub_categoria_id?}','CategoriaController@profesionalesTipoCategoria')
																->name('sub.categoria.elegir.profesion');

Route::resource('categorias','CategoriaController');

Route::get('contratacion/profesional/{profesional}/perfil','ContratacionController@datosProfesional')
																->name('contatacion.profesional.perfil');


Route::get('contatacion/servicio/{servicio}/ver','ContratacionController@index')
																->name('contratacion.index');
																
Route::get('contatacion/servicio/{servicio}','ContratacionController@show')
																->name('contratacion.show');


Route::resource('contatacion/servicio/{servicio}','ContratacionController',['names' => 
																		[

																		 'create'  => 'contratacion.create',
																		 'store'  => 'contratacion.store'
																	
																		],
																		'only' => ['create','store'],
																		'except' => 'index'
																	
																	  ]);

Route::get('contratacion/servicio','ContratacionController@listado_contratacion')->name('contratacion.listado');


Route::get('contratacion/servicio/{servicio}/aceptar','ContratacionController@aceptar')->name('contratacion.aceptar');
Route::get('contratacion/servicio/{servicio}/cancelar','ContratacionController@cancelar')->name('contratacion.cancelar');


Route::get('contratacion/servicio/data/{estado}/ajax','ContratacionController@datos_listado_ajax')->name('contratacion.listado.ajax');

Route::post('comentario/nuevo/{servicio}','ComentarioController@ajax_store')->name('comentario.nuevo');
Route::get('contratacion/servicio/comentario/like/{servicio}','ComentarioController@like')->name('comentario.like');
Route::get('contratacion/servicio/comentario/dislike/{servicio}','ComentarioController@dislike')->name('comentario.dislike');


Route::get('contratacion/ajax/{servicio}','ComentarioController@CantidadComentarios')->name('comentario.cantidad');