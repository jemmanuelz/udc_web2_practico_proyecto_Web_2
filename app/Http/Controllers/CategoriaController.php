<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Categoria_Servicio as Categoria;
use App\TipoProfesion as Tipo;
use App\Sub_Categoria_Servicio as Sub;


class CategoriaController extends Controller
{


    public function profesionalesTipoCategoria($sub_categoria)
    {
        //busca los tipos de profesiones que se indico para mostrarlo en la vista
        $tipo_profesiones=Sub::traer_profesiones($sub_categoria);

        return view('categoria.vista_index',compact('tipo_profesiones'));
    }


   
}
