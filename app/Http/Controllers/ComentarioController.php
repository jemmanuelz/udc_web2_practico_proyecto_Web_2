<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;
use App\Servicio;
use Carbon\Carbon as Carbon;
use App\Comentario;

class ComentarioController extends Controller
{
    

    public function like(Servicio $servicio)
    {
      //verifica que el usuario que esta logueado no sea el propio profesional
        if( $servicio->verificar_auto_contratacion() ){
           
           return view('contratacion.servicio.partials.like_dislike',compact('servicio'))->withErrors(['errors' => "el usuario no puede auto darse likes"]); 
        }
        //agrega un like al servicio
        $servicio->dar_like();

       return view('contratacion.servicio.partials.like_dislike',compact('servicio'));
    }

    public function dislike(Servicio $servicio)
    {
       //verifica que el usuario que esta logueado no sea el propio profesional
        if( $servicio->verificar_auto_contratacion() ){
           
           return view('contratacion.servicio.partials.like_dislike',compact('servicio'))->withErrors(['errors' => "el usuario no puede auto darse dis likes"]); 
        }
        //agrega un dislike al servicio
        $servicio->dar_dislike();

       return view('contratacion.servicio.partials.like_dislike',compact('servicio'));

    }


    public function ajax_store(Servicio $servicio,Request $request)
    {
      //mediante ajax crea un comentario de un servicio

      //valida que no este vacia la decripcion del comentario
      $v=\Validator::make($request->toArray(),['descripcion' => 'required |max:200']);

      if($v->fails()){
      
        return view('contratacion.servicio.partials.comentarios_servicio',compact('servicio'))->withErrors($v);
        }

      
        //inicializa el usuario 
       $user_id= null;

       if(Auth::check()){
          $user_id=Auth::user()->id;
       }else{
         return view('contratacion.servicio.partials.comentarios_servicio',compact('servicio'))->withErrors(['errors' => 'debe logearse primero']);
       }
       //crea el comentario en caso que este logueado el usuario toma el valor sino se muestra como anonimo
        Comentario::crear($user_id,$servicio->id,$request->descripcion);

        return view('contratacion.servicio.partials.comentarios_servicio',compact('servicio'));
    }


    public function CantidadComentarios(Servicio $servicio)
    {
      //actualiza la cantidad de comentarios luego de la insercion de uno nuevo
        return '<span class="pull-right text-muted">'.$servicio->comentarios->count().' Comentarios</span>';
    }
   


}
