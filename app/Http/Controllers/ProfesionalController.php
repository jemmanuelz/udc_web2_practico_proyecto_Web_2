<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\ProfesionalResquest as ValidarProfesional;

use App\User as Usuario;
use Auth;

class ProfesionalController extends Controller
{
   
   
  
    public function create()
    {
        $usuario=Auth::user();
        //se obtiene el usuario logiado
        return view('profesional.formulario.create',compact('usuario'));
    }

   
    public function store(ValidarProfesional $request)
    {
        //se registra al usuario como profesional
        Auth::user()->crear_profesional($request);

        return redirect()->route('usuario.datos');
    }

   
    public function show($id)
    {
        //busca al usuario logiado para mostrar sus datos como profesional
        $usuario=Auth::user();
        $read=true;

        return view('profesional.formulario.show',compact('usuario','read'));

    }

  
    public function edit($id)
    {
        $usuario=Auth::user();
        return view('profesional.formulario.edit',compact('usuario'));
    }

   
    public function update(ValidarProfesional $request, $id)
    {
        //actualiza los datos como profesional de un usuario
        Auth::user()->actualizar_profesional($request);
         
        return redirect()->route('usuario.datos');
    }

   
    public function destroy($id)
    {   
        //se da de baja como profesional a  un usuario
        Auth::user()->profesional->baja();
        return redirect()->route('usuario.datos');
    }
}
