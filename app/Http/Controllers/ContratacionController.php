<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input as Input;
use App\Http\Requests;

use App\User as Usuario;
use App\Servicio;
use App\Contratacion_Servicio as Contratacion;
use App\Estado_Contratacion as Estado;
use Auth;


class ContratacionController extends Controller
{
    

    public function index(Servicio $servicio)
    {
     
       //trae el usuario de un servicio 
       $usuario=$servicio->traer_usuario();

       return view('contratacion.servicio.index',compact('servicio','usuario'));
    }




    public function listado_contratacion()
    {
        //lista todos los estado de una contratacion para luego hacer un filtrado en la vista
        $estados=Estado::listado();
        return view('contratacion.index',compact('estados'));
    }



    public function datos_listado_ajax(Estado $estado)
    {
        //datos que actualiza la vista

        //valida antes de mostrar en la vista
        //si el usuario no es profesional
         if(empty(\Auth::user()->profesional)){
            return view('contratacion.partials.tabla_contratacion');
        }

        $profesiones= \Auth::user()->profesional->profesion;
        // si el usuario no tiene profesiones asignadas
        if(empty($profesiones)){
            return view('contratacion.partials.tabla_contratacion');
        }
        $servicios= Servicio::whereIn('profesion_id',$profesiones->lists('id'))->get()->lists('id');

        //sino tiene servicios registrados
         if(empty($servicios)){
             return view('contratacion.partials.tabla_contratacion');
        }
        $contrataciones=Contratacion::whereIn('servicio_id',$servicios)->where('estado_contratacion_id',$estado->id)->get();
        //si no tiene niguna contratacion
        if(empty($contrataciones->first())){
           
             return view('contratacion.partials.tabla_contratacion');
        }

        
        return view('contratacion.partials.tabla_contratacion',compact('contrataciones'));

    }




    public function datosProfesional($usuario_id)
    {
        //busca al usuario para ver un perfil para una posible contraracion donde se muestra los datos del servicio
        $usuario= Usuario::buscar($usuario_id);
        $read=true;
        return view('contratacion.profesional.index',compact('usuario','read'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Servicio $servicio)
    {
        //valida los datos ingresdos antes de el acceder a la vista del formulario para crea una contratacion

        $rules=[
                'id'   => 'exists:servicios,id'
                ];

        $mensajes=[

                'id.exists'   => ' El servicio ingresado no esta registrado',
            
        ];



        $v=\Validator::make($servicio->toArray(),$rules,$mensajes);

        if ( $v->fails() ) {
            
             return redirect()->route('contratacion.index',$servicio->id)->withErrors($v); 
        }


        //verifica que no se pueda auto contratar
        
        if( $servicio->verificar_auto_contratacion() ){
           
            return redirect()->route('contratacion.index',$servicio->id)->withErrors(['errors' => "el usuario no puede autocontratarse"]); 
        }


        return view('contratacion.servicio.formulario.create',compact('servicio') );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Servicio $servicio,Request $request)
    {

        //reglas de validacion
        $rules=[
                'puntuacion_servicio'   => 'numeric|required',
                'precio'                => 'numeric|digits_between:1,5'
                ];
        //mesajes a mostrar
        $mensajes=[

                'puntuacion_servicio.numeric'   => ' la puntuacion para el servicio debe ser un numero',
                'puntuacion_servicio.required'  => 'se necesita la puntuacion del servicio',
                'precio.numeric'                => 'el precio consignado para el servicio debe ser un numero',
                'precio.digits_between'         => 'las cifras del precio debe ser de 1  como minimo y no superar las 5 cifras'

        ];


            //valida los datos ingreados
        $v=\Validator::make($request->toArray(),$rules,$mensajes);

        //caso de error
        if ( $v->fails() ) {
            
             return redirect()->route('contratacion.index',$servicio->id)->withErrors($v); 
        }

        //valida el precio
        if ( $request->precio > 0 ) {
            
            return redirect()->route('contratacion.index',$servicio->id)->withErrors(['errors' => 'el precio del servicio debe ser positivo']); 
        }
        //valida que el servicio a guardar exista
        if(! $servicio->exits() ){
             return redirect()->route('contratacion.index',$servicio->id)->withErrors(['errors' => 'el servicio solicitado no esta registrado']); 
        }


        //crea una contratacion
        Contratacion::crear($servicio->id,$request );


        return redirect()->route('contratacion.index',$servicio->id); 
    }

  
    public function aceptar(Contratacion $contratacion)
    {
        $contratacion->aceptada();

       // return redirect()->route('contratacion.listado');
      
    }


    public function cancelar(Contratacion $contratacion)
    {
        $contratacion->cancelada();

        //  return redirect()->route('contratacion.listado');

    }
    
}
