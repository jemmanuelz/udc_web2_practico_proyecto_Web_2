<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\ServicioResquest as ValidarServicio;


use App\Profesional;
use App\Servicio;
use App\Tipo_Servicio as Tipo;
use App\Ubicacion_Servicio as Ubicacion;
use App\Profesion;


use Auth;

class ServicioController extends Controller
{
    

    public function index()
    {
    	$profesional=Auth::user()->profesional;


        $servicios= Servicio::whereIn('profesion_id',$profesional->profesion->lists('id')->toArray() )->get();

        return view('servicio.index',compact('servicios','profesional'));

    }

    public function create()
    {

    	$profesional=Auth::user()->profesional;
    	$profesiones=$profesional->profesion()->with('tipo_profesion')->get()->lists('tipo_profesion.nombre','id');
    	$tipo_servicios=Tipo::all()->lists('valor','id');
    	$ubicaciones=Ubicacion::all()->lists('ciudad','id');
    
        return view('servicio.formulario.create',compact('profesiones','profesional','tipo_servicios','ubicaciones'));

    }


    public function ServicioProfesionDeterminada($id)
    {
        $profesional=Auth::user()->profesional;
        $profesiones=$profesional->profesion()->with('tipo_profesion')->get()->lists('tipo_profesion.nombre','id');
        $tipo_servicios=Tipo::all()->lists('valor','id');
        $ubicaciones=Ubicacion::all()->lists('ciudad','id');
        $profesion=Profesion::find($id);

        return view('servicio.formulario.create',compact('profesiones','profesional','tipo_servicios','ubicaciones','profesion'));
    }
   
    public function store(ValidarServicio $request)
    {
        $profesional=Auth::user()->profesional;
    
        Servicio::create($request->all());


        return redirect()->route('servicio.index') ;
    }

   
    public function show($id)
    {
       
        $read=true;
        $profesional=Auth::user()->profesional;
        $profesiones=$profesional->profesion()->with('tipo_profesion')->get()->lists('tipo_profesion.nombre','id');
        $tipo_servicios=Tipo::all()->lists('valor','id');
        $ubicaciones=Ubicacion::all()->lists('ciudad','id');
        $servicio=Servicio::find($id);
    
        return view('servicio.formulario.show',compact('profesiones','profesional','tipo_servicios','ubicaciones','servicio','read'));
      

    }

  
    public function edit($id)
    {

        $profesional=Auth::user()->profesional;
        $profesiones=$profesional->profesion()->with('tipo_profesion')->get()->lists('tipo_profesion.nombre','id');
        $tipo_servicios=Tipo::all()->lists('valor','id');
        $ubicaciones=Ubicacion::all()->lists('ciudad','id');
        $servicio=Servicio::find($id);
    
        return view('servicio.formulario.edit',compact('profesiones','profesional','tipo_servicios','ubicaciones','servicio'));

        
    }

    
    public function update(ValidarServicio $request, $id)
    {
        Servicio::find($id)->update($request->all() );

        return redirect()->route('servicio.index') ;
    }

   
    public function destroy($id)
    {
        
    }

}
