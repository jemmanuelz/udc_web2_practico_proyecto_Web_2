<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\User as Usuario;
use Auth;


class UsuarioController extends Controller
{
    
    public function datosUsuarios()
    {
                
        $usuario=Auth::user();
        $read=true;
        return view('usuario.index',compact('usuario','read'));

    }
    

    public function datosUsuariosEdit()
    {
                
        $usuario=Auth::user();
        return view('usuario.formulario.datos_usuario',compact('usuario'));

    }

    public function datosUsuariosUpdate(Request $rqt)
    {
                
        $usuario=Auth::user();
        $usuario->update($rqt->all());
        
        if (isset($usuario->perfil_usuario)) {
             $usuario->perfil_usuario()->update($rqt->only(
                                                            'nombre_completo',
                                                            'telefono',
                                                            'domicilio'
                                                        ));
        }

        if (isset($usuario->profesional)) {
               $usuario->profesional()->update($rqt->only(
                                                                'cuil',
                                                                'telefono_alternativo'
                                                             ));
        }
       
        return redirect()->route('usuario.datos');

    }

    public function show($id)
    {
    	$usuario=Auth::user();
        $read=true;
        return view('usuario.formulario.show',compact('usuario','read'));
    }

    public function edit($id)
    {
        
        $usuario=Auth::user();

        return view('usuario.formulario.edit',compact('usuario'));

    }

    public function update($id,Request $rqt)
    {
        
        $usuario=Auth::user()->update($rqt->all());	
        return redirect()->route('usuario.datos');
    }

  

    public function destroy($id){
        Auth::user()->baja();
    }


}
