<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\PerfilUsuarioResquest as ValidarPerfil;
use App\Http\Requests;
use Auth;
//use App\User as Usuario;

class PerfilUsuarioController extends Controller
{
  
  
  
    public function create()
    {
        $usuario=Auth::user();

        return view('perfil_usuario.formulario.create',compact('usuario'));
    }

  
    public function store(ValidarPerfil $request)
    {

        //se valida antes los datos y el modelo crea el perfil para ese usuario
        Auth::user()->crear_perfil($request);

        return redirect()->route('usuario.datos');
    }

 
    public function show($id)
    {
        //muestra los datos del perfil del usuario
        $usuario=Auth::user();
        $read=true;

        return view('perfil_usuario.formulario.show',compact('usuario','read'));


    }

 
    public function edit($id)
    {   

        $usuario=Auth::user();
        return view('perfil_usuario.formulario.edit',compact('usuario'));
    }

    public function update($id,ValidarPerfil $request)
    {
        //actualiza los datos del usuario
        $usuario=Auth::user();
        $usuario->actualizar_perfil( $request );
         
        return redirect()->route('usuario.datos');
    }

 
    public function destroy($id)
    {   
        //da de baja al peril del usuario
          Auth::user()->perfil_usuario->baja();
           return redirect()->route('usuario.datos');
    }
}
