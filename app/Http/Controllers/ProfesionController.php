<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Profesional;
use App\Profesion;
use App\Tipo_Profesion;
use Auth;
use App\Http\Requests\ProfesionResquest as ValidarProfesion;



class ProfesionController extends Controller
{
   
    public function index()
    {
        //buscar el profesional que tiene registrado el usuario para luego en el  vista traer sus profesiones
        $profesional=Auth::user()->profesional;
        return view('profesion.index',compact('profesional'));
    }

    public function create()
    {
        $profesional=Auth::user()->profesional;
        //filtra para que el profesional no repita un profesion ya registrada
        $tipo_profesion= Tipo_Profesion::habilesProfesional($profesional);

      
        return view('profesion.formulario.create',compact('profesional','tipo_profesion'));

    }

   
    public function store(ValidarProfesion $request)
    {

        //crea una nueva profesion para el profesional
        Profesion::crear(Auth::user()->profesional,$request);

        return redirect()->route('profesional.profesion.index'); 
    }

   
    public function show($id)
    {
        //busca el profesional para ver todos sus datos
        $profesional=$usuario=Auth::user()->profesional;
   
        $tipo_profesion=Tipo_Profesion::all()->lists('nombre','id');

        $profesion=Profesion::find($id);
        $read=true;


        return view('profesion.formulario.show',compact('profesional','tipo_profesion','profesion','read'));

    }

  
    public function edit($id)
    {
        $profesional= Auth::user()->profesional;
     
        $tipo_profesion=Tipo_Profesion::habiles();

        $profesion=Profesion::find($id);


        return view('profesion.formulario.edit',compact('profesional','tipo_profesion','profesion'));
    }

    
    public function update(ValidarProfesion $request, $id)
    {
        //actualiza la profesion de un profesional
        Profesion::find($id)->actualizar( $request);
       

        return redirect()->route('profesional.profesion.index'); 
    }   

   
    public function destroy(Profesion $profesion)
    {   
        //baja de una profesion
        $profesion->baja();
         return redirect()->route('profesional.profesion.index');
    }
}
