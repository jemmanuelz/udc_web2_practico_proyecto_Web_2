<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tipo_Servicio extends Model
{
    

    protected $table = 'tipos_servicios';
  
   	protected $fillable = [

  						              'id',
                           	'valor'
                           
                           ];

  	protected $guarded = ['id'];

  	public function servicio()
    {
      return $this->hasMany('App\Servicio');
    }
    
}
