<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Estado_Contratacion as Estado;

class Contratacion_Servicio extends Model
{
    
    protected $table = 'contrataciones_servicios';
  
   	protected $fillable = [

  						              'id',
                            'fecha',
                            'puntuacion_servicio',
                            'precio',
                            'user_id',
                            'servicio_id',
                            'estado_contratacion_id'
                           ];

  	protected $guarded = ['id'];


    public function servicio()
    {
      return $this->belongsTo('App\Servicio','servicio_id','id');
    }

    public function usuario()
    {
      return $this->belongsTo('App\User','user_id','id');
    }

    public function estado_contratacion()
    {
      return $this->belongsTo('App\Estado_Contratacion','id','estado_contratacion_id');
    }


    static function crear($servicio,$datos)
    {
      if( \Auth::check() ){
        $usuario=\Auth::user()->id;
      }
      
    
      $datos['estado_contratacion_id'] = Estado::pendiente();
      $datos['fecha']=Carbon::now()->format('Y-m-d H:s:i');
      $datos['servicio_id'] =$servicio;
      $datos['user_id'] = $usuario;

      Contratacion_Servicio::create($datos->all());


    }


    public function scopePendientes($query)
    {
       
      return $query->where('estado_contratacion_id',Estado::pendiente())->get();

    }


    public function scopeCancelada()
    {
      $this->estado_contratacion_id=Estado::cancelada();
      $this->save();
    }


     public function scopeAceptada()
    {
      $this->estado_contratacion_id=Estado::aceptada();
      $this->save();
    }


    public function es_pendiente()
    {
      if($this->estado_contratacion_id == Estado::pendiente() ){
        return true;
      }else{
        return false;
      }
    }



}
