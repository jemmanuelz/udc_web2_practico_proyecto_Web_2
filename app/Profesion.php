<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profesion extends Model
{
    //

	protected $table = 'profesiones';
  
   	protected $fillable = [

  						    'id',
                  'experiencia',
  						    'tipo_profesion_id',
  						    'profesional_id'
  						   
                         ];

  	protected $guarded = ['id'];

    public function tipo_profesion()
    {
      return $this->belongsTo('App\Tipo_Profesion');
    }

    public function profesional()
    {
      return $this->belongsTo('App\Profesional');
    }

    public function servicios()
    {
      return $this->hasMany('App\Servicio')->where('activo',true);
    }


    static function crear( $profesional , $datos  )
    {    
      $profesional->profesion()->create($datos->all());
    }

    public function actualizar( $datos  )
    {    
      $this->update($datos->all());
    }


    public function scopeBaja()
    {
      $this->activo=false;
      $this->save();
    }

}
