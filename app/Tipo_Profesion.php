<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tipo_Profesion extends Model
{
    //
	 protected $table = 'tipo_profesiones';
  
   	protected $fillable = [
   							            'id',
                            'nombre',
                                                	                           
                           ];

     public function sub_categoria_servicios()
    {
      return $this->belongsToMany('App\Sub_Categoria_Servicio','sub_categoria_tipo_profesion','tipo_profesion_id','sub_categoria_servicio_id');
    }

    public function profesion()
    {
      return $this->hasMany('App\Profesion','tipo_profesion_id','id');
    }


    public function scopeHabilesProfesional($query,$profesional)
    {
      $profesiones_registradas=$profesional->profesion()->get()->lists('tipo_profesion_id')->toArray();
      return $query->whereNotIn('id',$profesiones_registradas)->lists('nombre','id');

    }


    public function scopeHabiles($query)
    {
     
      return $query->get()->lists('nombre','id');

    }
    
}
