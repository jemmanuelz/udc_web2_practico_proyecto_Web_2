<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sub_Categoria_Servicio extends Model
{
    
    protected $table = 'sub_categorias_servicios';
  
   	protected $fillable = [

  						              'id',
                            'nombre',
                            'categoria_servicio_id',
                         	  'valor'
                           
                           ];

  	protected $guarded = ['id'];


  	public function categoria_servicio()
    {
    	return $this->belongsTo('App\Categoria_Servicio');
    }

    public function tipo_profesion()
    {
    	return $this->belongsToMany('App\Tipo_Profesion','sub_categoria_tipo_profesion','sub_categoria_servicio_id','tipo_profesion_id');
    } 

    
     public function getValorAttribute($value){
      
      $value=str_replace("_"," ",$value);
      
      return $value;
        
    }


    public function scopeDeterminada($query,$value)
    {
      return $query->where('valor','LIKE',$value)->get()->first()->id;
    }

    public function scopeDeterminadaObj($query,$value)
    {
      return $query->where('valor','LIKE',$value)->get()->first();
    }



    public function scopeTraer_profesiones($query,$servicio)
    {
        return $query->where('id',$servicio)->get()->first()->tipo_profesion;
    }



}

