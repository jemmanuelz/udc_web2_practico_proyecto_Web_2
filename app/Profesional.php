<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profesional extends Model
{
    

    protected $table = 'profesionales';
  
   	protected $fillable = [

  						    'id',
  						    'cuil',
                  'activo',
                  'user_id',
                  'telefono_alternativo'

                         ];

  	protected $guarded = ['id'];


    public function usuario()
    {
      return $this->belongsTo('App\User','user_id','id');
    }

    public function profesion()
    {
      return $this->hasMany('App\Profesion')->where('activo',true);
    }
  

    public function publicaciones_destacadas()
    {
      return $this->hasMany('App\Publicacion_Destacada_Servicio');
    }

    public function setTelefonoAlternativoAttribute($value)
    {

      $value=trim($value,"_");
      $value=str_replace(")","",$value);
      $value=str_replace("(","",$value);
      $value=str_replace("0+","",$value);

      $this->attributes['telefono_alternativo'] = str_replace("4-","",$value);

    }
    //-----mutators cuil---//
    public function setCuilAttribute($value)
    {

      $value=trim($value,"_");
      $this->attributes['cuil'] = str_replace("-","",$value);

    }


    public function getCreatedAtAttribute($value)
    {

     return date("d-m-Y h:i:s",strtotime($value));

    }

    





    public function calcularReputacion()
    {
         $promedio_profesiones=0;
         $promedio_servicios=0;
         $promedio_servicio_like=1;
         $promedio_servicios_dislike=1;
         $promedio_servicios_contratacion=1;

        if($this->profesion->isEmpty()){
         
          $promedio_profesiones= 1;
          
           $porcentaje=$promedio_servicios+$promedio_profesiones;

        }else{

           $promedio_profesiones= $this->profesion->count();
        


          foreach ($this->profesion as $p ) {
            $promedio_servicios+= $p->servicios->count(); 
            foreach ($p->servicios as $servicio) {
              $promedio_servicio_like+=$servicio->like;
              $promedio_servicios_dislike+=$servicio->dislike;
              $promedio_servicios_contratacion+=$servicio->contrataciones_servicios->count();
            }
        

          }

           $porcentaje=$promedio_servicios+$promedio_servicio_like+$promedio_servicios_dislike+$promedio_servicios_contratacion;

        

        }

       
         return ($promedio_profesiones*$porcentaje)/100;
    }



    public function scopeBaja()
    {
      $this->activo=false;
       $this->save();
    }



}

