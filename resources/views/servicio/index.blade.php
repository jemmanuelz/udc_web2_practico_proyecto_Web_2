@extends('layouts.template')

@section('htmlheader_title')
    Profesional|{{  isset($profesional->usuario->perfil_usuario)?$profesional->usuario->perfil_usuario->nombre_completo : $profesional->usuario->nick}}|Servicios|Listado
@endsection


@section('contentheader_title')
   {{isset($profesional->usuario->perfil_usuario)?$profesional->usuario->perfil_usuario->nombre_completo : $profesional->usuario->nick}}
@endsection

@section('contentheader_description') 
    |Servicios|Listado
@endsection
       



@section('main-content')

    
<div>
    <a href="{{ route('servicio.create') }}" class="btn btn-success"><i class="fa fa-edit"></i>Agregar Nueva</a>
</div>

<br>

<div class="col-sm-3">
    <select id="servicioSelect" class="form-control"><option value="All">Servicios</option></select>      
</div><br><br>


<div class="col-sm-3">
    <select id="profesionSelect" class="form-control"><option value="All">Profesion</option></select>      
</div><br><br>


<div class="col-sm-3">
    <select id="tipo_servicioSelect" class="form-control"><option value="All">Tipo Servicio</option></select>      
</div><br><br>





 <table id="tabla_servicios" class="table table-bordered table-striped dataTable display" role="grid" cellspacing="0" width="100%" data-toggle="dataTable" data-form="deleteForm">
     <thead>
     <tr class="bg-info">
        
        <th aria-sort="descending" aria-label="Rendering engine: activate to sort column ascending" colspan="1" rowspan="1"  tabindex="0" class="sorting_desc">Servicio</th>
        
        <th aria-sort="descending" aria-label="Rendering engine: activate to sort column ascending" colspan="1" rowspan="1"  tabindex="0" class="sorting_desc">Profesion</th>
        
        <th aria-sort="descending" aria-label="Rendering engine: activate to sort column ascending" colspan="1" rowspan="1"  tabindex="0" class="sorting_desc">Tipo Servicio</th>
        
        <th aria-sort="descending" aria-label="Rendering engine: activate to sort column ascending" colspan="1" rowspan="1"  tabindex="0" class="sorting_desc">Cotizable</th>

        <th aria-sort="descending" aria-label="Rendering engine: activate to sort column ascending" colspan="1" rowspan="1"  tabindex="0" class="sorting_desc">Ver</th>
        
        <th aria-sort="descending" aria-label="Rendering engine: activate to sort column ascending" colspan="1" rowspan="1"  tabindex="0" class="sorting_desc">modificar</th>
        
        <th aria-sort="descending" aria-label="Rendering engine: activate to sort column ascending" colspan="1" rowspan="1"  tabindex="0" class="sorting_desc">baja</th>
       
       


     </tr>
     </thead>
     <tfoot>
     </tfoot>
    
     <tbody>

        @foreach ($servicios as $servicio)
        
            <tr>
                <td>{{ $servicio->nombre }}</td>
                <td>{{ $servicio->profesion->tipo_profesion->nombre }}</td>
                <td>{{ $servicio->tipo_servicio->valor }}</td>

                 <td>

                    @if($servicio->es_contizado) 
                      
                        <span class="glyphicon glyphicon-ok"></span></a>
                      
                    @else

                        <span class="glyphicon glyphicon-remove"></span></a>

                    @endif

                 </td>

               

                <td>
                    <a href="{{route('servicio.show',$servicio->id)}}" class="btn btn-primary"><span class="glyphicon glyphicon-eye-open"></span></a>
                </td>

                <td>
                    <a href="{{route('servicio.edit',$servicio->id)}}" class="btn btn-warning "><span class="glyphicon glyphicon-pencil"></a>
                </td>

                <td>                   
                    {!! Form::model($servicio, ['method' => 'delete', 'route' => ['servicio.destroy',$servicio->id], 'class' =>'form-inline form-delete']) !!}
                  
                    {!!Form::button('<span class="glyphicon glyphicon-trash"></span>', array('type' => 'submit', 'class' => 'btn btn-danger delete','name' => 'delete_modal'))!!}
                           
                    {!! Form::close() !!}
                </td>

            
                
            </tr>
             
       
     @endforeach
   
     
    
     </tbody>
   


 </table>
    


   <a href="{{ route('usuario.datos' ) }}" class="btn btn-danger btn">volver</a>
   
@endsection



{{-- 
 
@section('script')

    <script>

 
        $ = jQuery.noConflict();
        $(document).ready(function() {
        
        var table = $('#tabla_servicios').DataTable({
           // "serverSide":true,
            "info":     true,
            "language": {
 
              "sProcessing": "Procesando...", 
              "sLengthMenu": "Mostrar _MENU_ registros",
              "sZeroRecords": "No se encontraron resultados",
              "sEmptyTable": "Ningún dato disponible en esta tabla",
              "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
              "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
              "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
              "sInfoPostFix": "",
              "sSearch": "Buscar:",
              "sUrl": "",
              "sInfoThousands": ",",
              "sLoadingRecords": "Cargando...",
              "oPaginate": {
                  "sFirst": "Primero",
                  "sLast": "Último",
                  "sNext": "Siguiente",
                  "sPrevious": "Anterior"
 
                },
                "oAria": {
                    "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
 
                }
 
          },

        //order: [[2, 'asc']],
                    
                    initComplete: function() {
                        var api = this.api();
                        var select = $('#servicioSelect');
                        api.column(0).data().unique().sort().each( function ( d, j ) {
                            select.append( '<option value="'+d+'">'+d+'</option>' )
                        } );   
                  
                        var select = $('#profesionSelect');
                        api.column(1).data().unique().sort().each( function ( d, j ) {
                            select.append( '<option value="'+d+'">'+d+'</option>' )
                        } );   
                  
                        var select = $('#tipo_servicioSelect');
                        api.column(2).data().unique().sort().each( function ( d, j ) {
                            select.append( '<option value="'+d+'">'+d+'</option>' )
                        } );   
                    }


                });

                $('#servicioSelect').change(function() {
                    var val = $.fn.dataTable.util.escapeRegex(
                        $(this).val()
                    );
                    table.column(0)
                        .search( val == 'All' ? '' : '^'+val+'$', true, false )
                        .draw();
                });

                  $('#profesionSelect').change(function() {
                    var val = $.fn.dataTable.util.escapeRegex(
                        $(this).val()
                    );
                    table.column(1)
                        .search( val == 'All' ? '' : '^'+val+'$', true, false )
                        .draw();
                });

                $('#tipo_servicioSelect').change(function() {
                    var val = $.fn.dataTable.util.escapeRegex(
                        $(this).val()
                    );
                    table.column(2)
                        .search( val == 'All' ? '' : '^'+val+'$', true, false )
                        .draw();
                });



        $('table[data-form="deleteForm"]').on('click', '.form-delete', function(e){
            e.preventDefault();
            var $form=$(this);
            $('#confirm').modal({ backdrop: 'static', keyboard: false })
                .on('click', '#delete-btn', function(){
                $form.submit();
            });
        });
    });
    </script>
@endsection

--}}