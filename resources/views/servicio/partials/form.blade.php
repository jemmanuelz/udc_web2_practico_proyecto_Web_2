	<div class="form-group row">
        
            {!! Form::label('ubicacion_servicio_id', 'Ubicacion:'.(isset($read)? '' : '*'),['class'=>'col-sm-1 form-control-label']) !!}
        
            <div class="col-sm-6">
           
                   
                    {!! Form::select('ubicacion_servicio_id',['seleccione una ubicacion ']+$ubicaciones->toArray(),(isset($servicio)? $servicio->ubicacion_servicio_id:null),['class'=>'form-control','id' => 'ubicacion_servicio_id' ,(isset($read)? 'disabled': '')  ]) !!}
                               
            </div>
    </div>



  <div class="form-group row">
        
            {!! Form::label('tipo_servicio_id', 'Tipo de Servicio:'.(isset($read)? '' : '*'),['class'=>'col-sm-1 form-control-label']) !!}
        
            <div class="col-sm-6">
           
                   
                    {!! Form::select('tipo_servicio_id',['seleccione un tipo de servicio']+$tipo_servicios->toArray(),(isset($servicio)? $servicio->tipo_servicio_id:null),['class'=>'form-control','id' => 'tipo_servicio_id',(isset($read)? 'disabled': '')   ]) !!}
                               
            </div>
    </div>

  

     <div class="form-group row">
        
            {!! Form::label('profesion', 'Profesion:'.(isset($read)? '' : '*'),['class'=>'col-sm-1 form-control-label']) !!}
        
            <div class="col-sm-6">
                

                @if(isset($profesion))
                    {!! Form::select('profesion_id',['seleccione una profesion ']+$profesiones->toArray(),(isset($profesion)? $profesion->id:null),['class'=>'form-control','id' => 'profesion_id',(isset($profesion)? 'disabled': '')  ]) !!}
                  
                  {!! Form::hidden('profesion_id',$profesion->id) !!}

                 @else

                 	 {!! Form::select('profesion_id',['seleccione una profesion ']+$profesiones->toArray(),(isset($servicio)? $servicio->profesion->id:null),['class'=>'form-control','id' => 'profesion_id',(isset($servicio)? 'disabled': '')  ]) !!}

                 @endif
                               
            </div>
    </div>
	
     <div class="form-group row">
        {!! Form::label('nombre', 'Nombre identificador del servicio:'.(isset($read)? '' : '(opcional)'),['class'=>'col-sm-1 form-control-label']) !!}
            
            <div class="col-sm-6">
                {!! Form::text('nombre',isset($servicio)? $servicio->nombre:null,['class'=>'form-control','id' => 'nombre' , (isset($read)? 'readonly': '') ]) !!}
            </div>
    </div>


    <div class="form-group row">
        {!! Form::label('descripcion', 'Descripcion del servicio:'.(isset($read)? '' : '(opcional)'),['class'=>'col-sm-1 form-control-label']) !!}
            
            <div class="col-sm-6">
                {!! Form::textarea('descripcion',isset($servicio)? $servicio->descripcion:null,['class'=>'form-control','id' => 'descripcion','placeholder' => 'Descripcion del servicio,materiales necesarios,duracion,forma de cotizacion(por hora, por dia, etc)' , (isset($read)? 'readonly': '') ]) !!}
            </div>
    </div>


	<div class="form-group row">
        {!! Form::label('es_contizado', 'Tiene cotizacion?:'.(isset($read)? '' : '(opcional)'),['class'=>'col-sm-1 form-control-label']) !!}
            
            <div class="col-sm-6">
				{!! Form::checkbox('es_contizado',true,isset($servicio)? $servicio->es_contizado: false,['id' => 'es_contizado' , (isset($read)? 'disabled': '') ])!!}
            </div>
    </div>



<div id="tick_es_cotizado" 
@if(isset($servicio)) 
    @if($servicio->es_contizado)
         ''
     @else 
     style="display: none" 
    @endif 
@else 
    style="display: none"
@endif  >
	<div class="form-group row">
        {!! Form::label('precio', 'Precio:'.(isset($read)? '' : '(opcional)'),['class'=>'col-sm-1 form-control-label']) !!}
            
            <div class="col-sm-6">
				{!! Form::text('precio',isset($servicio)? $servicio->precio:0,['class'=>'form-control','id' => 'precio' , (isset($read)? 'readonly': '') ]) !!}
            </div>
    </div>
</div>


  
{{-- 
 
@section('script')

    <script>

 
     $ = jQuery.noConflict();
	$(document).ready(function() {
	
	 $('#es_contizado').on('click', function(e){
        
        //var nro_ticket = e.target.value;
             
        var bol= $('#es_contizado').prop('checked');


        if (  bol ) {
            $('#tick_es_cotizado').show('fast');                    
        }else{
			$('#tick_es_cotizado').hide('fast');
            $('#precio').val(0);
                    
        }          
                    
     });

	});

</script>
@endsection

--}}