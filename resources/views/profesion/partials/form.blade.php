 <div class="form-group row">
        
            {!! Form::label('tipo_profesion_id', 'Profesion:'.(isset($read)? '' : '*'),['class'=>'col-sm-1 form-control-label']) !!}
        
            <div class="col-sm-6">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-phone fa-fw w3-margin-right w3-large w3-text-teal"></i></span>
                
                    {!! Form::select('tipo_profesion_id',['seleccione un tipo de profesion']+$tipo_profesion->toArray(),(isset($profesion)? $profesion->tipo_profesion_id:null),['class'=>'form-control','id' => 'tipo_profesion_id',(isset($profesion)? 'disabled': '')  ]) !!}
                </div>                   
            </div>
    </div>

  

 <div class="form-group row">
        
            {!! Form::label('experiencia', 'Experiencia:',['class'=>'col-sm-1 form-control-label']) !!}

             <div class="col-sm-6">	
            @if(isset($read))


                <div class="progress-group">
                 
                    <span class="progress-number"><b>{{ $profesion->experiencia}}</b>/100</span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-red" style="width: {{ $profesion->experiencia}}%"></div>
                    </div>
                  </div>             
            @else
        
          

				<input data-value="-135,15" style="display: none;" value="-135,15" class="slider form-control" data-slider-min="0" data-slider-max="100" data-slider-step="5" data-slider-value="{{isset($profesion)?$profesion->experiencia:0}}" data-slider-orientation="horizontal" data-slider-selection="before" data-slider-tooltip="show" data-slider-id="red" id="experiencia" name="experiencia" type="number"  >
                                
            

            @endif
            </div>
    </div>



@section('script')
<script type="text/javascript">
	$(function () {
    
    $('.slider').slider();

  });

</script>
@endsection
