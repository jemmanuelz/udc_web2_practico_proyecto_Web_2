@extends('layouts.template')

@section('htmlheader_title')
    Profesional|{{   $profesional->usuario->nick}}|Profesion|Nuevo
@endsection


@section('contentheader_title')
   {{isset($profesional->usuario->perfil_usuario)?$profesional->usuario->perfil_usuario->nombre_completo : $profesional->usuario->nick}}
@endsection

@section('contentheader_description') 
    |Profesion|Nuevo
@endsection
       



@section('main-content')

 {!! Form::model($profesional,['route'=>['profesional.profesion.store'],'id'=>'usuario_profesion']) !!}

	<div class="nav-tabs-custom">
		<ul class="nav nav-tabs">
            <li class="active">
                 <a aria-expanded="false" href="#tab_3" data-toggle="tab"><i class="fa fa-briefcase fa-fw w3-margin-right w3-large w3-text-teal"></i>Profesion</a>
            </li>
        </ul>
		<div class="tab-content">

   
        	@include('profesion.partials.form')
        
 

 		</div>


    	<div class="box-footer">
        
     		 
                {!! Form::submit('Guardar', ['class' => 'btn btn-success ','id' => 'guardar']) !!}
                 <a href="{{ route('profesional.profesion.index' ) }}" class="btn btn-danger btn">volver</a>         

         </div>


    </div>
             
   {!! Form::close() !!}

@endsection