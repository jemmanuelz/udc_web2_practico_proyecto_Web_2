@extends('layouts.template')



@section('htmlheader_title')
    Profesional|{{  isset($profesional->usuario->perfil_usuario)?$profesional->usuario->perfil_usuario->nombre_completo : $profesional->usuario->nick}}|Profesion|Listado
@endsection


@section('contentheader_title')
   {{isset($profesional->usuario->perfil_usuario)?$profesional->usuario->perfil_usuario->nombre_completo : $profesional->usuario->nick}}
@endsection

@section('contentheader_description') 
    |Profesion|Listado
@endsection
       



@section('main-content')

    
<div>
    <a href="{{ route('profesional.profesion.create') }}" class="btn btn-success"><i class="fa fa-edit"></i>Agregar Nueva</a>

    @if($profesional->profesion()->count() > 0)
     <a href="{{route('servicio.index') }}" class="btn btn-success "><i class="fa fa-edit"></i> Administrar Servicio</a>
    @endif
</div>

<br>

<div class="col-sm-3">

    <select id="profesionSelect" class="form-control"><option value="All">Profesiones</option></select>      
</div><br><br>

   

  

 <table id="tabla_profesiones" class="table table-bordered table-striped dataTable display" role="grid" cellspacing="0" width="100%" data-toggle="dataTable" data-form="deleteForm">
     <thead>
     <tr class="bg-info">
        <th aria-sort="descending" aria-label="Rendering engine: activate to sort column ascending" colspan="1" rowspan="1"  tabindex="0" class="sorting_desc">Profesion</th>
        <th aria-sort="descending" aria-label="Rendering engine: activate to sort column ascending" colspan="1" rowspan="1"  tabindex="0" class="sorting_desc">Servicio</th>
        <th aria-sort="descending" aria-label="Rendering engine: activate to sort column ascending" colspan="1" rowspan="1"  tabindex="0" class="sorting_desc">Ver</th>
        <th aria-sort="descending" aria-label="Rendering engine: activate to sort column ascending" colspan="1" rowspan="1"  tabindex="0" class="sorting_desc">modificar</th>
        <th aria-sort="descending" aria-label="Rendering engine: activate to sort column ascending" colspan="1" rowspan="1"  tabindex="0" class="sorting_desc">baja</th>
       
       


     </tr>
     </thead>
     <tfoot>
     </tfoot>
    
     <tbody>

        @foreach ($profesional->profesion as $profesion)
        
            <tr>
                <td>{{ $profesion->tipo_profesion->nombre }}</td>

                <td>
                    <a href="{{route('profesion.servicio.create',[$profesion->id])}}" class="btn btn-success "><span class="glyphicon glyphicon-plus">Servicio</a>
                </td>


                <td>
                    <a href="{{route('profesional.profesion.show',[$profesion->id])}}" class="btn btn-primary"><span class="glyphicon glyphicon-eye-open"></span></a>
                </td>

                <td>
                    <a href="{{route('profesional.profesion.edit',[$profesion->id])}}" class="btn btn-warning "><span class="glyphicon glyphicon-pencil"></a>
                </td>

                <td>                   
                    {!! Form::model($profesional, ['method' => 'delete', 'route' => ['profesional.profesion.destroy',$profesion->id], 'class' =>'form-inline form-delete']) !!}
                  
                    {!!Form::button('<span class="glyphicon glyphicon-trash"></span>', array('type' => 'submit', 'class' => 'btn btn-danger delete','name' => 'delete_modal'))!!}
                           
                    {!! Form::close() !!}
                </td>

            
                
            </tr>
             
       
     @endforeach
   
     
    
     </tbody>
   


 </table>
    


   <a href="{{ route('usuario.datos') }}" class="btn btn-danger btn">volver</a>

@endsection




 
@section('script')

    <script>

 
        $ = jQuery.noConflict();
        $(document).ready(function() {
    {{--     
        var table = $('#tabla_profesiones').DataTable({
           // "serverSide":true,
            "info":     true,
            "language": {
 
              "sProcessing": "Procesando...", 
              "sLengthMenu": "Mostrar _MENU_ registros",
              "sZeroRecords": "No se encontraron resultados",
              "sEmptyTable": "Ningún dato disponible en esta tabla",
              "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
              "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
              "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
              "sInfoPostFix": "",
              "sSearch": "Buscar:",
              "sUrl": "",
              "sInfoThousands": ",",
              "sLoadingRecords": "Cargando...",
              "oPaginate": {
                  "sFirst": "Primero",
                  "sLast": "Último",
                  "sNext": "Siguiente",
                  "sPrevious": "Anterior"
 
                },
                "oAria": {
                    "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
 
                }
 
          },

        //order: [[2, 'asc']],
                    
                    initComplete: function() {
                        var api = this.api();
                        var select = $('#profesionSelect');
                        api.column(0).data().unique().sort().each( function ( d, j ) {
                            select.append( '<option value="'+d+'">'+d+'</option>' )
                        } );   
                    }
                });

                $('#profesionSelect').change(function() {
                    var val = $.fn.dataTable.util.escapeRegex(
                        $(this).val()
                    );
                    table.column(0)
                        .search( val == 'All' ? '' : '^'+val+'$', true, false )
                        .draw();
                });

        });

--}}
               $('table[data-form="deleteForm"]').on('click', '.form-delete', function(e){
                    e.preventDefault();
                    var $form=$(this);
                    $('#confirm').modal({ backdrop: 'static', keyboard: false })
                        .on('click', '#delete-btn', function(){
                        $form.submit();
                    });
                 });
        });

    </script>
@endsection

