    
        <div class="form-group row">
        	
        	{!! Form::label('nombre_completo', 'Nombre Completo:'.(isset($read)? '' : '*'),['class'=>'col-sm-1 form-control-label']) !!}
            
            <div class="col-sm-6">
            	<div class="input-group">
                <span class="input-group-addon"><i class="fa fa-user fa-fw w3-margin-right w3-xxlarge w3-text-teal"></i></span>
            	{!! Form::text('nombre_completo',isset($usuario->perfil_usuario)? $usuario->perfil_usuario->nombre_completo:null,['class'=>'form-control','id' => 'nombre_completo' , (isset($read)? 'readonly': '') ]) !!}
				</div>
            </div>
    	</div>

    	<div class="form-group row">
        
        	{!! Form::label('telefono', 'Telefono:'.(isset($read)? '' : '*'),['class'=>'col-sm-1 form-control-label']) !!}
        
        	<div class="col-sm-6">
            	<div class="input-group">
               		<span class="input-group-addon"><i class="fa fa-phone fa-fw w3-margin-right w3-large w3-text-teal"></i></span>
                
                	{!! Form::text('telefono',isset($usuario->perfil_usuario)? $usuario->perfil_usuario->telefono:null,['class'=>'form-control','id' => 'telefono',(isset($read)? 'readonly': '')  ]) !!}
            	</div>                   
       		</div>
    	</div>

    	

    	<div class="form-group row">
        	
        	{!! Form::label('domicilio', 'Domicilio:'.(isset($read)? '' : '*'),['class'=>'col-sm-1 form-control-label']) !!}
            
            <div class="col-sm-6">
            	<div class="input-group">
            	<span class="input-group-addon"><i class="fa fa-home fa-fw w3-margin-right w3-large w3-text-teal"></i></span>
				{!! Form::text('domicilio',isset($usuario->perfil_usuario)? $usuario->perfil_usuario->domicilio:null,['class'=>'form-control','id' => 'domicilio' , (isset($read)? 'readonly': '') ]) !!}
				</div>
            </div>
    	</div>



{{--  

@section('script')
<script type="text/javascript">
    
$(function() {

      $("#telefono").inputmask({
            mask: "+54(999)999-9999"
        });

         $("#telefono_alternativo").inputmask({
            mask: "0+(999)4-999999"
        });
  $("#cuil").inputmask({
            mask: "99-99999999-9"
        })

    
   
    // body...
});



</script>

@endsection

--}}