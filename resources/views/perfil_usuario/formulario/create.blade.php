@extends('layouts.template')

@section('htmlheader_title')
    Usuario|{{$usuario->nick}}|Perfil|Ver
@endsection


@section('contentheader_title')
   Usuario: {{$usuario->nick}}
@endsection

@section('contentheader_description') 
    Perfil 
@endsection
       



@section('main-content')

 {!! Form::model($usuario,['route'=>['usuario.perfil.store',$usuario->id],'id'=>'usuario_perfil']) !!}

	<div class="nav-tabs-custom">
		<ul class="nav nav-tabs">
            <li class="active">
                <a aria-expanded="false" href="#tab_1" data-toggle="tab"><i class="fa  fa-group fa-fw w3-margin-right w3-large w3-text-teal"></i>Perfil Usuario</a>
            </li>
        </ul>
		<div class="tab-content">

   
        	@include('perfil_usuario.partials.form')
        
 

 		</div>


    	<div class="box-footer">
        
     		 
                {!! Form::submit('Guardar', ['class' => 'btn btn-success ','id' => 'guardar']) !!}
                <a href="{{ route('usuario.datos' ) }}" class="btn btn-danger btn">volver</a>         

         </div>


    </div>
             
   {!! Form::close() !!}

@endsection