@extends('layouts.app')

@section('htmlheader_title')
	Bienvenida
@endsection

@section('header')
    <link href="{{ asset('/css/elementos/barra_categoria.css') }}" rel="stylesheet" type="text/css" />
@endsection
 
 
   

@section('main-content')

 @include('layouts.partials.nav_categorias') <!-- barra de categorias !-->
	<div class="container spark-screen">
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				<div id="vista_categorias" class="panel panel-default">
					<div class="panel-heading">Home</div>

					<div class="panel-body">
						{{ trans('adminlte_lang::message.logged') }}
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

{{-- 
@section('script')
	<script type="text/javascript">
	
	$(function() {

			$("ul.sub a").on('click',function (e){
				e.preventDefault();
				
				$('#vista_categorias').load(this.href);
			
			});

	});


	</script>

@endsection

--}}