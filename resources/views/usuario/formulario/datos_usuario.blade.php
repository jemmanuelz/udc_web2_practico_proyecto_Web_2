@extends('layouts.template')

@section('htmlheader_title')
    Usuario|Datos
@endsection


@section('contentheader_title')
   <i class="fa fa-street-view fa-fw w3-margin-right w3-large w3-text-teal"></i> Usuario: {{$usuario->nick}}
@endsection

@section('contentheader_description') 
    datos 
@endsection
       



@section('main-content')


    {!! Form::model($usuario,['method' => 'PUT','route'=>['usuario.datos.update',$usuario->id],'id'=>'usuario']) !!}


 <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="">
                <a aria-expanded="false" href="#tab_1" data-toggle="tab"><i class="fa fa-user fa-fw w3-margin-right w3-large w3-text-teal"></i>Datos Usuario</a>
            </li>


            <li class="">
                <a aria-expanded="false" href="#tab_2" data-toggle="tab"><i class="fa  fa-group fa-fw w3-margin-right w3-large w3-text-teal"></i>Datos Personales</a>
            </li>

            <li class="">
                <a aria-expanded="false" href="#tab_3" data-toggle="tab"><i class="fa fa-briefcase fa-fw w3-margin-right w3-large w3-text-teal"></i>Datos Laborales</a>
            </li>
           
        </ul>

        <div class="tab-content">
         <!-- tab-pane -->
            <div class="tab-pane active" id="tab_1">
                
                @include('usuario.partials.form')
                 <a href="{{route('usuario.show',$usuario->id)}}" class="btn btn-info">Ver</a>
                
            </div> <!-- /.tab-pane -->

              <!-- /.tab-pane -->
            <div class="tab-pane" id="tab_2">

                  @if(isset($usuario->perfil_usuario))
                     @include('perfil_usuario.partials.form')
                      <a href="{{route('usuario.perfil.show',[$usuario->id,$usuario->perfil_usuario->id])}}" class="btn btn-info">Ver</a>  
                  @else

                    <a href="{{ route('usuario.perfil.create',$usuario->id) }}" class="btn btn-success">Agregar</a>
                    <br><br>

                     <p><i class="fa fa-exclamation-circle fa-fw w3-margin-right w3-large w3-text-teal"></i>no tiene un perfil registrado</p>


             
                @endif

                 

              
            </div>
              <!-- /.tab-pane -->
            <div class="tab-pane" id="tab_3">


                @if(isset($usuario->profesional))
                  
                 @include('profesional.partials.form')
                  <a href="{{route('usuario.profesional.show',[$usuario->id,$usuario->profesional->id])}}" class="btn btn-info">Ver</a> 
                @else
                 <a href="{{ route('usuario.profesional.create',$usuario->id) }}" class="btn btn-success">Agregar</a>
                 <br><br>

                  <p><i class="fa fa-exclamation-circle fa-fw w3-margin-right w3-large w3-text-teal"></i>no esta registrado como profesional</p>

  
                  @endif


            </div>
            
        </div>
            <!-- /.tab-content -->

        <div class="box-footer">
        
       
          

                {!! Form::submit('Guardar', ['class' => 'btn btn-success ','id' => 'guardar']) !!}
           

                <a href="{{ route('usuario.datos') }}" class="btn btn-danger btn">volver</a>

     

         </div>

    </div>
          <!-- nav-tabs-custom -->

   

     {!! Form::close() !!}

      
 


@endsection