@extends('layouts.template')

@section('htmlheader_title')
    Usuario|Ver
@endsection


@section('contentheader_title')
   Usuario: {{$usuario->nick}}
@endsection

@section('contentheader_description') 
    datos 
@endsection
       



@section('main-content')


	<div class="nav-tabs-custom">
		<ul class="nav nav-tabs">
            <li class="active">
                <a aria-expanded="false" href="#tab_1" data-toggle="tab">Datos Usuario</a>
            </li>
        </ul>
		<div class="tab-content">
    		@include('usuario.partials.form')
    	</div>


    	<div class="box-footer">
        
     		 @if(!(isset($read)))
                {!! Form::submit('Guardar', ['class' => 'btn btn-success ','id' => 'guardar']) !!}
                 <a href="{{ url('/') }}" class="btn btn-danger btn">cancelar</a>
            @else

                <a href="{{route('usuario.edit',$usuario->id)}}" class="btn btn-warning ">Editar</a>

                 <a href="{{ route('usuario.datos' ) }}" class="btn btn-danger btn">volver</a>

            @endif
            

         </div>


    </div>
             


@endsection