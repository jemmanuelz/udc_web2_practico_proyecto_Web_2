@extends('layouts.template')


@section('header')
    <link href="{{ asset('/css/elementos/usuario.css') }}" rel="stylesheet" type="text/css" />
@endsection
 

@section('htmlheader_title')
    Usuario|Datos
@endsection



@section('contentheader_title')
    <img class="img-circle" src="{{asset('/img/user8-128x128.jpg')}}" alt="User Image" >   Usuario: {{$usuario->nick}}
@endsection

@section('contentheader_description') 
    datos 
@endsection
       



@section('main-content')

  <div class="w3-container">  

      @yield('botones_acceso')

  </div>
  <br>

<div class="w3-row-padding">
  
    <!-- Left Column -->
    <div class="w3-third">
    
      <div class="w3-white w3-text-grey w3-card-4">
        <div class="w3-display-container">

              
        </div>
       <div class="w3-container">

        <h2 class="w3-text-grey w3-padding-16"><i class="fa fa-user  fa-fw w3-margin-right w3-xxlarge w3-text-teal"></i>Datos Usuario</h2>
        
            <p><i class="fa fa-street-view fa-fw w3-margin-right w3-large w3-text-teal"></i>{{$usuario->nick}}</p>
            <p><i class="fa fa-envelope fa-fw w3-margin-right w3-large w3-text-teal"></i>{{$usuario->email}}</p>


                <hr>

   


         <h2 class="w3-text-grey w3-padding-16"><i class="fa fa-group fa-fw w3-margin-right w3-xxlarge w3-text-teal"></i>Perfil</h2>


        @if(isset($usuario->perfil_usuario))



           <p><i class="fa fa-male fa-fw w3-margin-right w3-large w3-text-teal"></i>{{$usuario->perfil_usuario->nombre_completo}}</p>

    
           <p><i class="fa fa-phone fa-fw w3-margin-right w3-large w3-text-teal"></i>{{$usuario->perfil_usuario->telefono}}</p>


          

            <p><i class="fa fa-home fa-fw w3-margin-right w3-large w3-text-teal"></i>{{$usuario->perfil_usuario->domicilio}}</p>
            

     @if(Auth::check())
      @if(Auth::user()->id == $usuario->id )
              <div style="float:right">
            
              
          {!! Form::model($usuario->perfil_usuario, ['method' => 'delete', 'route' => ['usuario.perfil.destroy',$usuario->perfil_usuario->id], 'class' =>'form-inline form-delete']) !!}
                  
                    {!!Form::button('<span class="glyphicon glyphicon-trash"> perfil</span>', array('type' => 'submit', 'class' => 'btn btn-danger delete','name' => 'delete_modal'))!!}
                           
                    {!! Form::close() !!}
         

          </div>

        @endif
    @endif
            @else
            <hr>
            
    		    
            <p><i class="fa fa-exclamation-circle fa-fw w3-margin-right w3-large w3-text-teal"></i>no tiene un perfil registrado</p>
           
             
          @endif


         
          
         
          <hr>
         
          @if(isset( $usuario->profesional ))
          <p class="w3-large"><b><i class="fa fa-asterisk fa-fw w3-margin-right w3-text-teal"></i>Puntuacion</b></p>
          <p>Reputacion</p>
          <div class="w3-progress-container w3-round-xlarge w3-small">
            <div class="w3-progressbar w3-round-xlarge w3-teal" style="width:{{ $usuario->profesional->calcularReputacion() }}%">
              <div class="w3-center w3-text-black">{{ $usuario->profesional->calcularReputacion() }}%</div>
            </div>
          </div>
        @endif
          <br>
        </div>
      </div><br>

    <!-- End Left Column -->
    </div>

    <!-- Right Column -->
    <div class="w3-twothird">
    
      <div class="w3-container w3-card-2 w3-white w3-margin-bottom">
      
         @if(isset($usuario->profesional))         
            
             

        <div class="w3-container">
           <h3 class="w3-text-grey w3-padding-16"><i class="fa fa-briefcase fa-fw w3-margin-right w3-xxlarge w3-text-teal"></i>Datos/Profesional</h3>

    @if(Auth::check())
      @if(Auth::user()->id == $usuario->id )
          <div style="float:right">
            
              {!! Form::model($usuario->profesional, ['method' => 'delete', 'route' => ['usuario.profesional.destroy',$usuario->profesional->id], 'class' =>'form-inline form-delete']) !!}
                  
                    {!!Form::button('<span class="glyphicon glyphicon-trash"> profesional </span>', array('type' => 'submit', 'class' => 'btn btn-danger delete','name' => 'delete_modal'))!!}
                           
                    {!! Form::close() !!}
         

          </div>
         @endif 
    @endif 
          <p>Cuil:</p>
          <h6 class="w3-text-teal"><i class="fa fa-check-square-o fa-fw w3-margin-right"></i>{{$usuario->profesional->cuil}}</h6>
          <br>
          <p>Telefono Alternativo:</p>
          <h6 class="w3-text-teal"><i class="fa fa-phone fa-fw w3-margin-right"></i>{{$usuario->profesional->telefono_alternativo}}</h6>
          <br>


          <p>Fecha de registro:</p>
          <h6 class="w3-text-teal">
          <i class="fa fa-calendar fa-fw w3-margin-right">            
            </i>{{$usuario->profesional->created_at}}</h6>
          <br>
          <br>

          @yield('lista_profesiones')
         

          
        
          

          
        </div>

        @endif

         
      </div>


      
      </div>

    


    <!-- End Right Column -->
    </div>

      <div>
    
        <a href="{{ url('/') }}" class="btn btn-danger btn">volver</a>
      </div>
    
  <!-- End Grid -->
  </div>
  



@endsection

