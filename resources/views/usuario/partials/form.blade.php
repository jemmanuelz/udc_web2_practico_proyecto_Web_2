	<div class="form-group row">
        {!! Form::label('nick', 'Nick:'.(isset($read)? '' : '*'),['class'=>'col-sm-1 form-control-label']) !!}
            
            <div class="col-sm-6">
				{!! Form::text('nick',isset($usuario)? $usuario->nick:null,['class'=>'form-control','id' => 'nick' , (isset($read)? 'readonly': '') ]) !!}
            </div>
    </div>

    <div class="form-group row">
        
        {!! Form::label('email', 'Email:'.(isset($read)? '' : '*'),['class'=>'col-sm-1 form-control-label']) !!}
        
        <div class="col-sm-6">
            <div class="input-group">
                <span class="input-group-addon">@</span>
                
                {!! Form::email('email',isset($usuario)? $usuario->email:null,['class'=>'form-control','id' => 'email',(isset($read)? 'readonly': '')  ]) !!}
            </div>                   
        </div>
    </div>