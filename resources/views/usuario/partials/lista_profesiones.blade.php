 @foreach($usuario->profesional->profesion as $profesion)
            <p><i class="fa fa-briefcase fa-fw w3-margin-right w3-large w3-text-teal"></i>{{$profesion->tipo_profesion->nombre}}  </p>
          
            <p>Experiencia:</p>
            <div class="w3-progress-container w3-round-xlarge w3-small ">
              <div class="w3-progressbar w3-round-xlarge w3-teal" style="width:{{ $profesion->experiencia }}%">
                <div class="w3-center w3-text-white">{{ $profesion->experiencia }}%</div>
              </div>
            </div>

            <hr>


            @foreach($profesion->servicios as $servicio )
               <p><i class="fa-fw w3-margin-right w3-large w3-text-teal">{{$servicio->nombre}}</i>(servicio)<a href="{{route('servicio.show',$servicio->id)}}">Detalle</a> </p>
            @endforeach

       
          @endforeach