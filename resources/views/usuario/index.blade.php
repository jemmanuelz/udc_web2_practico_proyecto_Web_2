@extends('usuario.partials.template_index_usuario_contrataciones_perfil')


@section('botones_acceso')
  
  @if(isset($usuario->profesional)) <!-- cambiar por rol -->
          @include('layouts.partials.acceso.usuario.botones.profesional')
  @else
          @include('layouts.partials.acceso.usuario.botones.usuario')
  @endif

@endsection



@section('lista_profesiones')
 @if(isset($usuario->profesional)) 
	@include('usuario.partials.lista_profesiones')
@endif
@endsection