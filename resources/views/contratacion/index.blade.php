@extends('layouts.template')

@section('htmlheader_title')
    Profesional|{{  isset(Auth::user()->perfil_usuario)?Auth::user()->perfil_usuario->nombre_completo : Auth::user()->nick}}|Servicios|Listado
@endsection


@section('contentheader_title')
   {{isset(Auth::user()->perfil_usuario)? Auth::user()->perfil_usuario->nombre_completo : Auth::user()->nick}}
@endsection

@section('contentheader_description') 
    |Contrataciones|Listado
@endsection
       



@section('main-content')

   <div class="col-sm-3">
    {!! Form::select('estado_profesion_id',['' => 'seleccion las Contrataciones']+$estados->toArray(),null,['class'=>'form-control','id' => 'estado_contratacion' ]   ) !!}
    </div><br><br>
    
    <div id="div_tabla_contratacion">
        
    @include('contratacion.partials.tabla_contratacion')

    </div>


   <a href="{{ route('usuario.datos' ) }}" class="btn btn-danger btn">volver</a>
   
@endsection



{{-- 

@section('script')
  <script type="text/javascript">
  
  $(function() {

      $("ul.sub a").on('click',function (e){
        e.preventDefault();
        
        $('#vista_categorias').load(this.href);
      
      });

  });


  </script>

@endsection

--}}