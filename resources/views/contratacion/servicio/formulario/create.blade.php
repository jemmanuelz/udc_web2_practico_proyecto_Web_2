@extends('layouts.template')

@section('htmlheader_title')
  Servicio|{{$servicio->nombre}}|{{$servicio->profesion->profesional->usuario->nick}}
@endsection


@section('contentheader_title')
   Servicio: {{$servicio->nombre}}
@endsection

@section('contentheader_description') 
    Contratacion 
@endsection
       



@section('main-content')

 {!! Form::model($servicio,['route'=>['contratacion.store',$servicio->id],'id'=>'contratacion_servicio']) !!}

	<div class="nav-tabs-custom">
		<ul class="nav nav-tabs">
            <li class="active">
                <a aria-expanded="false" href="#tab_1" data-toggle="tab"><i class="fa fa-usd fa-fw w3-margin-right w3-large w3-text-teal"></i>Contratacion</a>
            </li>
        </ul>
		<div class="tab-content">

   
        @include('contratacion.servicio.partials.form')
        
 

 		</div>


    	<div class="box-footer">
        
     		 
                {!! Form::submit('Guardar', ['class' => 'btn btn-success ','id' => 'guardar']) !!}
                <a href="{{ url('/') }}" class="btn btn-danger btn">volver</a>         

         </div>


    </div>
             
   {!! Form::close() !!}

@endsection