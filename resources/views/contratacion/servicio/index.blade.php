@extends('layouts.template')


@section('header')
    <link href="{{ asset('/css/elementos/usuario.css') }}" rel="stylesheet" type="text/css" />
@endsection
 

@section('htmlheader_title')
  Servicio|{{$servicio->nombre}}|{{$servicio->profesion->profesional->usuario->nick}}
@endsection


@section('contentheader_title')
   Servicio: {{$servicio->nombre}}
@endsection

@section('contentheader_description') 
    Perfil 
@endsection
       



@section('main-content')




<div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="ion ion-ios-cart-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Contrataciones</span>
              <span class="info-box-number">{{$servicio->contrataciones_servicios->count()}} </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>

<div class="col-md-3 col-sm-9 col-xs-12">
<div class="info-box bg-aqua">
            <span class="info-box-icon"><i class="fa fa-star-o"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Experiencia</span>
            

              <div class="progress">
                <div class="progress-bar" style="width: {{$servicio->profesion->experiencia}}%"></div>
              </div>
                  <span class="progress-description">
                    {{$servicio->profesion->experiencia}}%  como {{$servicio->profesion->tipo_profesion->nombre }}
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
</div>
<div id="likes" >
  @include('contratacion.servicio.partials.like_dislike')
</div>

<div>
	<p>Comentarios</p>
</div>

<div class="col-sm-6  col-md-5  col-xs-12">



          <!-- Box Comment -->
          <div class="box box-widget">
            <div class="box-header with-border">
              <div class="user-block">
               <img class="img-circle" src="{{asset('/img/user8-128x128.jpg')}}" alt="User Image"> 
                <span class="username">Profesional <a href="#">{{$servicio->profesion->profesional->usuario->nick}}</a></span>
                <span class="description">{{$servicio->created_at}}</span>
              </div>
              <!-- /.user-block -->
              <div class="box-tools  pull-right">
               
                <button type="button" class="btn btn-box-tool " data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
               
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body" style="display: block;" >
             <p>{{$servicio->nombre}}</p>
             <!-- <img class="img-responsive pad" src="../dist/img/photo2.png" alt="Photo"> -->

          
              @include('contratacion.servicio.partials.descripcion_servicio')
         
         @if(!Auth::guest())
              <a type="button" class="btn btn-default btn-xs " id="like" href="{{route('comentario.like',$servicio->id)}}"><i class="fa fa-thumbs-up" ></i> Me gusta</a>

              <a type="button" class="btn btn-default btn-xs "  id="dislike" href="{{route('comentario.dislike',$servicio->id)}}" ><i class="fa fa-thumbs-down"></i>No me gusta</a>
          @endif
              <div id="comentario_cantidad">
             
                 <span class="pull-right text-muted">{{$servicio->comentarios->count()."  Comentarios"}}</span>
               
              </div>
             
            </div>
            <!-- /.box-body -->
         
            <div class="box-footer box-comments direct-chat-messages"  id="comentario_servicio" style="display: block;" >
            
                <!-- User image -->
              
                @include('contratacion.servicio.partials.comentarios_servicio')
               
          
              <!-- /.box-comment -->
              </div>
            @if(!Auth::guest())
            <!-- /.box-footer -->
            <div class="box-footer" style="display: block;" >
              <form action="{{route('comentario.nuevo',$servicio->id)}}" method="post" id="comentario-create">
               <!-- <img class="img-responsive img-circle img-sm" src="../dist/img/user4-128x128.jpg" alt="Alt Text"> -->
             {{ csrf_field() }}
                <div class="img-push">
                  <div class="input-group">
                      <input name="descripcion" id="descripcion_serv" placeholder="Ingrese su comentario..." class="form-control" type="text">
                          <span class="input-group-btn">
                            <button type="submit" class="btn btn-warning btn-flat">Send</button>
                          </span>

                    </div>
                   
                </div>
              </form>
            </div>
            <!-- /.box-footer -->
          @endif
          </div>
          <!-- /.box -->
        </div>



<div class="col-md-7" style=" float: right;">
<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Perfil de <b>{{$usuario->nick}}</b> </h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            		@include('contratacion.servicio.partials.datos_usuario')
      
            </div>
            <!-- /.box-body -->
            <div class="box-footer ">
            @if(Auth::guest())
              <p><i class="fa fa-exclamation-circle "></i> Tiene que registrarse para hacer una Contratacion</p>
            @else
               <a href="{{ route('contratacion.create',$servicio->id)}}" class="btn btn-sm btn-info btn-flat pull-right">Contratar</a>
            @endif
            </div>
            <!-- /.box-footer -->
          </div>

</div>

<div class="col-sm-6  col-md-5  col-xs-12" style="float:left;clear: left; ">


<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Servicios que puedan interesar de {{$usuario->nick}}</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
               @include('contratacion.servicio.partials.listado_servicio_profesional')
      
            </div>
            <!-- /.box-body -->
         
            <!-- /.box-footer -->
          </div>



</div>
</div>



@endsection



@section('script')

<script type="text/javascript">
  
  $(function() {

        $("#comentario-create").bind("submit",function(){

          $.ajax({
            type: $(this).attr("method"),
            url: $(this).attr("action"),
            data:$(this).serialize(),

            success: function(data){
              
                $("#comentario_cantidad").empty();
                $("#comentario_cantidad").load('{{route('comentario.cantidad',$servicio->id)}}');
                $("#comentario_servicio").html(data);
      
                $('#descripcion_serv').val('');

            },
            error: function(data){
               
                alert("Problemas al tratar de enviar el formulario");
            }
        });
       

      
        return false;

    });

  });


  </script>



@endsection

