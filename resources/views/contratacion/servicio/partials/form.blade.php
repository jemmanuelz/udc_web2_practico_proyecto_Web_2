<input data-value="135,5" style="display: none;"  data-step="1" class="slider form-control" data-slider-min="-10" data-slider-max="10" data-slider-step="1" data-slider-value="0"  data-slider-orientation="horizontal" data-slider-selection="before" data-slider-tooltip="show" data-slider-id="white" data-hasgrid="false"  id="puntuacion_servicio" name="puntuacion_servicio" type="number"  >

<div class="form-group row">
        {!! Form::label('precio', 'Precio:'.(isset($read)? '' : '(opcional)'),['class'=>'col-sm-1 form-control-label']) !!}
            
            <div class="col-sm-6">
            @if($servicio->es_contizado)
            	{!! Form::text('precio',isset($servicio)? $servicio->precio:0,['class'=>'form-control','id' => 'precio','readonly']) !!}
            @else
            	{!! Form::text('precio',isset($servicio)? $servicio->precio:0,['class'=>'form-control','id' => 'precio']) !!}
            @endif


			
            </div>
    </div>



@section('script')
<script type="text/javascript">
	$(function () {
    /* BOOTSTRAP SLIDER */
    $('.slider').slider();


  });

</script>
@endsection
