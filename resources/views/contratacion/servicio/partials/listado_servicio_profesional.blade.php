
@if(! $usuario->profesional->profesion()->get()->isEmpty() )
@foreach($usuario->profesional->profesion as $profesion)
 
    @if(!($profesion->servicios()->where('id','!=',$servicio->id)->get()->isEmpty())) 

            @foreach($profesion->servicios()->where('id','!=',$servicio->id)->get() as $serv )
               <p><i class="fa-fw w3-margin-right w3-large w3-text-teal">{{$serv->nombre}}</i>(servicio) <a href="{{ route('contratacion.index',['servicio' => $serv->id])}}">Contratar</a> </p>
            @endforeach


      @endif
@endforeach
@else


        <p><i class="fa fa-exclamation-circle "></i>no tiene un otros servicios registrados</p>
  @endif