  @if (count($errors) > 0)
        <div class="box-comment" id="comentarios" style="background-color: rgba(255, 0, 36, 0.13); " style="display: block;">
            <div class="comment-text" style="display: block;">
           
                <strong>Aviso!</strong>  revise lo siguiente<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
           
          </div>
        </div>
        @endif

<div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box bg-green">
            <span class="info-box-icon"><i class="fa fa-thumbs-o-up"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Me gusta</span>
              <span class="info-box-number">{{$servicio->like}}</span>

              <div class="progress">
                <div class="progress-bar" style="width: {{$servicio->like}}%"></div>
              </div>
                  <span class="progress-description">
                    
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>

<div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box bg-red">
            <span class="info-box-icon"><i class="fa fa-thumbs-o-down"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">No me gusta</span>
              <span class="info-box-number">{{$servicio->dislike}}</span>

              <div class="progress">
                <div class="progress-bar" style="width: {{$servicio->like}}%"></div>
              </div>
                  <span class="progress-description">
                   
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
