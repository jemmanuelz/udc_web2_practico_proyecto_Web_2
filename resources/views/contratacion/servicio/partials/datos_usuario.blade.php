  
 @if(isset($usuario->profesional))         
            
          


        <div class="w3-container">

      

          <h4 class="w3-text-grey w3-padding-16"><i class="fa fa-briefcase fa-fw w3-margin-right w3-text-teal"></i>Datos/Profesional</h4>

          <p>Cuil:</p>
          <h6 class="w3-text-teal"><i class="fa fa-check-square-o fa-fw w3-margin-right"></i>{{$usuario->profesional->cuil}}</h6>
          <br>
       

          <p>Fecha de registro:</p>
          <h6 class="w3-text-teal">
          <i class="fa fa-calendar fa-fw w3-margin-right">            
            </i>{{$usuario->profesional->created_at}}</h6>
          <br>
          <br>
          </div>
@endif          
        
      


  @if(isset($usuario->perfil_usuario))


           <h4 class="w3-text-grey w3-padding-16"><i class="fa fa-briefcase fa-fw w3-margin-right w3-text-teal"></i>Datos Personales</h4>
          <p><i class="fa fa-male fa-fw w3-margin-right w3-large w3-text-teal "></i>{{$usuario->perfil_usuario->nombre_completo}}</p>
  
        
          

            <p><i class="fa fa-home fa-fw w3-margin-right w3-large w3-text-teal"></i>{{$usuario->perfil_usuario->domicilio}}</p>
            
            
            @else
           
            
    		    
            <p><i class="fa fa-exclamation-circle "></i>no tiene un perfil registrado</p>
    
    
@endif

