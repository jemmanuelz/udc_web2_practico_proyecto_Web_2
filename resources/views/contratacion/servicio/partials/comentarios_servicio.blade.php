   @if (count($errors) > 0)
        <div class="box-comment" id="comentarios" style="background-color: rgba(255, 0, 36, 0.13); " style="display: block;">
            <div class="comment-text" style="display: block;">
           
                <strong>Aviso!</strong>  revise lo siguiente<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
           
          </div>
        </div>
        @endif

@if(!($servicio->comentarios->isEmpty())) 
@foreach($servicio->comentarios_ordenados() as $comentario)
<div class="box-comment" id="comentarios" style="display: block;">
 <div class="comment-text" style="display: block;">
    <span class="username">
    	@if(isset($comentario->usuario))
    	{{$comentario->usuario->nick}}
    	@else
    	-Anomino-
    	@endif
        <span class="text-muted pull-right">{{$comentario->fecha}}</span> 
   </span><!-- /.username -->
               {{$comentario->descripcion}}
</div>
</div>
@endforeach


     


@else
  <div class="box-comment" id="comentarios" style="display: block;">
	
	 <p><i class="fa fa-exclamation-circle "></i> No tiene comentarios registrados <i class="fa fa-frown-o "></i></p>
	 </div>
@endif


