@include('layouts.modal_Confirm')

<div class="col-sm-3">
    <select id="fechaSelect" class="form-control"><option value="All">Fecha</option></select>      
</div><br><br>

<div class="col-sm-3">
    <select id="servicioSelect" class="form-control"><option value="All">Servicios</option></select>      
</div><br><br>


<div class="col-sm-3">
    <select id="profesionSelect" class="form-control"><option value="All">Profesion</option></select>      
</div><br><br>


<div class="col-sm-3">
    <select id="tipo_servicioSelect" class="form-control"><option value="All">Tipo Servicio</option></select>      
</div><br><br>





 <table id="tabla_servicios" class="table table-bordered table-striped dataTable display" role="grid" cellspacing="0" width="100%" data-toggle="dataTable" data-form="deleteForm">


     <thead>
     <tr class="bg-info">

        <th aria-sort="descending" aria-label="Rendering engine: activate to sort column ascending" colspan="1" rowspan="1"  tabindex="0" class="sorting_desc">Fecha</th>
                
        <th aria-sort="descending" aria-label="Rendering engine: activate to sort column ascending" colspan="1" rowspan="1"  tabindex="0" class="sorting_desc">Servicio</th>
        
        <th aria-sort="descending" aria-label="Rendering engine: activate to sort column ascending" colspan="1" rowspan="1"  tabindex="0" class="sorting_desc">Profesion</th>
        
        <th aria-sort="descending" aria-label="Rendering engine: activate to sort column ascending" colspan="1" rowspan="1"  tabindex="0" class="sorting_desc">Tipo Servicio</th>


        <th aria-sort="descending" aria-label="Rendering engine: activate to sort column ascending" colspan="1" rowspan="1"  tabindex="0" class="sorting_desc">Cotizable</th>

        <th aria-sort="descending" aria-label="Rendering engine: activate to sort column ascending" colspan="1" rowspan="1"  tabindex="0" class="sorting_desc">Precio</th>

        <th aria-sort="descending" aria-label="Rendering engine: activate to sort column ascending" colspan="1" rowspan="1"  tabindex="0" class="sorting_desc">Usuario</th>

        <th aria-sort="descending" aria-label="Rendering engine: activate to sort column ascending" colspan="1" rowspan="1"  tabindex="0" class="sorting_desc">Puntuacion</th>

@if(isset($contrataciones))
    @if(  $contrataciones->first()->es_pendiente() )
        <th aria-sort="descending" aria-label="Rendering engine: activate to sort column ascending" colspan="2" rowspan="1"  tabindex="0" class="sorting_desc"> </th>
    @endif    
@endif  


     </tr>
     </thead>
     <tfoot>
     </tfoot>
    
     <tbody>
      @if(isset($contrataciones))
        @foreach ($contrataciones as $contratacion)
        

            <tr>
                <td>{{ $contratacion->fecha }}</td>
                <td>{{ $contratacion->servicio->nombre }}</td>
                <td>{{ $contratacion->servicio->profesion->tipo_profesion->nombre }}</td>
                <td>{{ $contratacion->servicio->tipo_servicio->valor }}</td>

                 <td>

                    @if($contratacion->servicio->es_contizado) 
                      
                        <span class="glyphicon glyphicon-ok"></span></a>
                      
                    @else

                        <span class="glyphicon glyphicon-remove"></span></a>

                    @endif

                 </td>
                <td>{{ $contratacion->precio}}</td>
                <td>{{ $contratacion->usuario->nick}}</td>
                <td>{{ $contratacion->puntuacion_servicio}}</td>
               
             @if($contrataciones->first()->es_pendiente() )
                <td>
                  <a href="{{route('contratacion.aceptar',$contratacion->id)}}" class="aceptar"><span class="glyphicon glyphicon-ok"></span> </a>                 
                </td>
                 <td>
                  <a href="{{route('contratacion.cancelar',$contratacion->id)}}" class="aceptar"><span class="glyphicon glyphicon-ok"></span> </a>                     
                </td>

             @endif



                
            </tr>
             
       
     @endforeach
   
 @endif
    
     </tbody>
   


 </table>
    




 
@section('script')

    <script>


     {{-- 
        var table = $('#tabla_servicios').DataTable({
           // "serverSide":true,
            "info":     true,
             "processing": true,
      
            "language": {
 
              "sProcessing": "Procesando...", 
              "sLengthMenu": "Mostrar _MENU_ registros",
              "sZeroRecords": "No se encontraron resultados",
              "sEmptyTable": "Ningún dato disponible en esta tabla",
              "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
              "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
              "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
              "sInfoPostFix": "",
              "sSearch": "Buscar:",
              "sUrl": "",
              "sInfoThousands": ",",
              "sLoadingRecords": "Cargando...",
              "oPaginate": {
                  "sFirst": "Primero",
                  "sLast": "Último",
                  "sNext": "Siguiente",
                  "sPrevious": "Anterior"
 
                },
                "oAria": {
                    "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
 
                }
 
          },

        //order: [[2, 'asc']],
                    
                    initComplete: function() {

                        var api = this.api();
                        var select = $('#fechaSelect');
                        api.column(0).data().unique().sort().each( function ( d, j ) {
                            select.append( '<option value="'+d+'">'+d+'</option>' )
                        } ); 
                        var api = this.api();
                        var select = $('#servicioSelect');
                        api.column(1).data().unique().sort().each( function ( d, j ) {
                            select.append( '<option value="'+d+'">'+d+'</option>' )
                        } );   
                  
                        var select = $('#profesionSelect');
                        api.column(2).data().unique().sort().each( function ( d, j ) {
                            select.append( '<option value="'+d+'">'+d+'</option>' )
                        } );   
                  
                        var select = $('#tipo_servicioSelect');
                        api.column(3).data().unique().sort().each( function ( d, j ) {
                            select.append( '<option value="'+d+'">'+d+'</option>' )
                        } );   
                    }


                });

                $('#fechaSelect').change(function() {
                    var val = $.fn.dataTable.util.escapeRegex(
                        $(this).val()
                    );
                    table.column(0)
                        .search( val == 'All' ? '' : '^'+val+'$', true, false )
                        .draw();
                });

                 $('#servicioSelect').change(function() {
                    var val = $.fn.dataTable.util.escapeRegex(
                        $(this).val()
                    );
                    table.column(1)
                        .search( val == 'All' ? '' : '^'+val+'$', true, false )
                        .draw();
                });


                  $('#profesionSelect').change(function() {
                    var val = $.fn.dataTable.util.escapeRegex(
                        $(this).val()
                    );
                    table.column(1)
                        .search( val == 'All' ? '' : '^'+val+'$', true, false )
                        .draw();
                });

                $('#tipo_servicioSelect').change(function() {
                    var val = $.fn.dataTable.util.escapeRegex(
                        $(this).val()
                    );
                    table.column(2)
                        .search( val == 'All' ? '' : '^'+val+'$', true, false )
                        .draw();
                });



       
      

--}}
         $('table[data-form="deleteForm"]').on('click', '.form-delete', function(e){
            e.preventDefault();
            var $form=$(this);
            $('#confirm').modal({ backdrop: 'static', keyboard: false })
                .on('click', '#delete-btn', function(){
                $form.submit();
            });
        });


        $('#estado_contratacion').change(function (e){
            var estado_contratacion_id = e.target.value;

            if(estado_contratacion_id != '' ){

              $('#div_tabla_contratacion').load("./servicio/data/"+estado_contratacion_id+"/ajax");
            // $.getScript("public/js/script.js");
           
            // table.ajax.reload("./servicio/data/"+estado_contratacion_id+"/ajax");
            }

       




    });
    </script>
@endsection