
<nav class="categoria">
  <ul class="primary">
  	@foreach($categorias as $categoria)
    <li class="media">
      <a aria-expanded="false" href="#">{{$categoria->valor}}</a>
      <ul class="sub">
      @foreach($categoria->sub_categorias as $sub)

        <li><a href="{{ route('sub.categoria.elegir.profesion',$sub->id)}}">{{$sub->valor}}</a></li>
        
        @endforeach
      </ul>
    </li>
    @endforeach
    
  </ul>
</nav>