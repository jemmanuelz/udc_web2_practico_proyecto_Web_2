{{--usuario,perfil--}}


<li class="treeview">
    <a href="#">
        <i class="fa fa-user"></i> 
        <span>Datos Personales</span> 
        <i class="fa fa-angle-left pull-right"></i>
    </a>


    <ul class="treeview-menu">
    
        <li>
            <a href="{{  route('usuario.datos')   }}">
                <i class="fa fa-circle-o text-yellow"></i>Usuario
            </a>
        </li>

   
        <li>
            <a href="{{ route('usuario.show',Auth::user()->id )   }}">
                <i class="fa fa-circle-o text-aqua"></i> Ver
            </a>
        </li>
   
    </ul>
</li>












<li class="treeview">
                    <a href="#">
                        <i class="fa fa-group"></i> 
                            <span>Perfil</span> 
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                    @if(!isset(Auth::user()->perfil_usuario))
                        <li>
                            <a href="{{  route('usuario.perfil.create')   }}">
                                <i class="fa  fa-user-plus "></i> Agregar/Nuevo
                            </a>
                        </li>
                    @endif
                        @if(isset(Auth::user()->perfil_usuario))

                        <li>
                            <a href="{{ route('usuario.perfil.show',Auth::user()->id )   }}">
                                <i class="fa fa-circle-o text-aqua"></i> Ver
                            </a>
                        </li>
                        @endif
                    </ul>
</li>

