{{--perfil y servicios profesional--}}

<li class="treeview">
    <a href="#">
        <i class="fa fa-briefcase"></i> 
            <span>Profesional</span> 
        <i class="fa fa-angle-left pull-right"></i>
    </a>
    
    <ul class="treeview-menu">
        @if(!isset(Auth::user()->profesional))
            <li>
                <a href="{{  route('usuario.profesional.create')   }}">
                    <i class="fa  fa-user-plus "></i> Agregar/Nuevo
                </a>
            </li>
        @else
			<li>
                <a href="{{ route('usuario.profesional.show',Auth::user()->profesional->id )   }}">
                    <i class="fa fa-circle-o text-aqua"></i> Ver
                </a>
            </li>
        @endif 
    </ul>
</li>
@if(isset(Auth::user()->profesional))
    @include('layouts.partials.acceso.usuario.link_servicio')
@endif