{{--servicios,publicaciones,cotrataciones,comentarios--}}


<li class="treeview">
    <a href="#">
        <i class="fa fa-suitcase"></i> 
            <span>Profesion</span> 
        <i class="fa fa-angle-left pull-right"></i>
    </a>
    
    <ul class="treeview-menu">

    		<li>
                <a href="{{ route('profesional.profesion.index' )   }}">
                    <i class="fa fa-circle-o text-aqua"></i> Listado
                </a>
            </li>
        
            <li>
                <a href="{{  route('profesional.profesion.create',Auth::user()->id)   }}">
                    <i class="fa  fa-circle-o text-yellow"></i> Agregar/Nuevo
                </a>
            </li>
      
			
  
    </ul>
</li>

@if(Auth::user()->profesional->profesion()->count() > 0)

<li class="treeview">
    <a href="#">
        <i class="fa fa-ship"></i> 
            <span>Servicio</span> 
        <i class="fa fa-angle-left pull-right"></i>
    </a>
    
    <ul class="treeview-menu">
        
            <li>
                <a href="{{  route('servicio.index')   }}">
                    <i class="fa  fa-circle-o  text-aqua"></i> Listado
                </a>
            </li>
        
			<li>
                <a href="{{ route('servicio.create',Auth::user()->profesional->id )   }}">
                    <i class="fa fa-circle-o text-yellow"></i> Agregar/Nuevo
                </a>
            </li>
         
    </ul>
</li>
@endif
