<a href="{{route('usuario.datos.edit')}}" class="btn btn-warning ">Editar</a> 
  
@if(!isset($usuario->perfil_usuario))
    <a href="{{ route('usuario.perfil.create') }}" class="btn btn-success">
         <i class="fa fa-edit"></i>Registrar Perfil</a>

@endif
  

 @if(!isset($usuario->profesional))

   <a class="btn btn-success"  href="{{route('usuario.profesional.create')}}">
                <i class="fa fa-edit"></i> Registrarse como Profesional
  </a>

@endif