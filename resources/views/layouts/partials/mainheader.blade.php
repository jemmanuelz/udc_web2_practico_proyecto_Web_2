<!-- Main Header -->
<header class="main-header">

    <!-- Logo -->
    <a href="{{ url('/') }}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>H</b>SOS</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Trabajo</b>Hecho SOS </span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
     @if (!(Auth::guest()))
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">{{ trans('adminlte_lang::message.togglenav') }}</span>
        </a>
    @endif
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- Messages: style can be found in dropdown.less-->
                @if (! Auth::guest())
   
                   <!-- Notifications Menu -->
                <li class="dropdown notifications-menu ">

                    <!-- Menu toggle button -->
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-bell-o"></i>
                        <span class="label label-warning">

                        @if(empty( Auth::user()->comentarios_de_hoy() ) ) 0

                        @else
                            {{Auth::user()->comentarios_de_hoy()->count() }} 
                        @endif 

                    
                        </span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">Comentarios</li>
                        <li>
                            <!-- Inner Menu: contains the notifications -->
                            <ul class="menu">

                           {{ Auth::user()->mostrar_comentarios() }}   
 
           {{--                 @if(empty( Auth::user()->comentarios_de_hoy() ) ) 
                                <i class="fa fa-users text-aqua"></i> no tiene comentarios hoy!!!

                        @else
                            
                               @for($i=0;$i <6 ;$i++)
                                <li>
                                    <a href="#">

                                        <i class="fa fa-users text-aqua"></i>  {{'servicio :'.Auth::user()->comentarios_de_hoy()->get($i)->servicio->profesion->tipo_profesion->nombre.'descripcion :'.Auth::user()->comentarios_de_hoy()->get($i)->descripcion }} 
                                    </a>
                                </li>

                            @endfor

                        @endif--}}


                                






                         
                            </ul>
                        </li>
                        <li class="footer"><a href="comentarios.index">Ver todos</a></li>
                    </ul>
                </li>
                 
                <!-- Tasks Menu -->
                <li class="dropdown tasks-menu">
                    <!-- Menu Toggle Button -->
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-flag-o"></i>
                        <span class="label label-success">
                   
                    @if(empty( Auth::user()->todas_contrataciones_pendientes() ) )
                             0



                        @else
                            {{Auth::user()->todas_contrataciones_pendientes()->count() }} 
                        @endif 

                     
                        </span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">
                           Contrataciones pendientes
                        </li>
                        <li>
                            <!-- Inner menu: contains the tasks -->
                            <ul class="menu">
                           {{Auth::user()->mostrar_contrataciones() }}  

                            </ul>
                        </li>
                        <li class="footer">
                            <a href="{{ route('contratacion.listado')}}">Ver todas las Contrataciones</a>
                        </li>
                    </ul>
                </li>
            @endif
                @if (Auth::guest())
                    <li><a href="{{ url('/register') }}">{{ trans('adminlte_lang::message.register') }}</a></li>
                    <li><a href="{{ url('/login') }}">{{ trans('adminlte_lang::message.login') }}</a></li>
                @else
                    <!-- User Account Menu -->
                    <li class="dropdown user user-menu">
                        <!-- Menu Toggle Button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <!-- The user image in the navbar-->
                            <img src="{{asset('/img/user2-160x160.jpg')}}" class="user-image" alt="User Image"/>
                            <!-- hidden-xs hides the username on small devices so only the image appears. -->
                            <span class="hidden-xs">{{ Auth::user()->nick }} {{ isset(Auth::user()->perfil_usuario )? Auth::user()->perfil_usuario->nombre_completo: null }} </span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- The user image in the menu -->
                            <li class="user-header">
                                <img src="{{asset('/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image" />
                                <p>
                                    {{ Auth::user()->nick }}
                                    <small>{{ trans('adminlte_lang::message.login') }} {{\Carbon\Carbon::now()->formatLocalized('%A %d %B %Y')}}</small>
                                </p>
                            </li>
                            <!-- Menu Body -->
                            <li class="user-body">
                                <div class="col-xs-4 text-center">
                                    <a href="{{ route('servicio.index')}}  ">{{ trans('adminlte_lang::message.servicios') }}</a>
                                </div>
                                <div class="col-xs-4 text-center">
                                    <a href="#">{{ trans('adminlte_lang::message.publicaciones') }}</a>
                                </div>
                                <div class="col-xs-4 text-center">
                                    <a href="{{ route('contratacion.listado')}}">{{ trans('adminlte_lang::message.contrataciones') }}</a>
                                </div>
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="{{ route('usuario.datos')}} " class="btn btn-default btn-flat">{{ trans('adminlte_lang::message.profile') }}</a>
                                </div>
                                <div class="pull-right">
                                    <a href="{{ url('/logout') }}" class="btn btn-default btn-flat">{{ trans('adminlte_lang::message.signout') }}</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                @endif

             
            </ul>
        </div>
    </nav>
</header>


