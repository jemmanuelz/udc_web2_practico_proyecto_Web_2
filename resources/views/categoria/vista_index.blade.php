<div  class="panel-heading">Profesionales y Servicios Correspodientes a la busqueda</div>

<div class="panel-body">

@if($tipo_profesiones->isEmpty())

	<p>No hay profesion registrada en la categoria</p>

@else

@foreach($tipo_profesiones as $tipo)

	<p>*{{$tipo->nombre}}</p>

	@if($tipo->profesion->isEmpty())
		<p>No hay profesion en la categoria</p>
	@endif

	@foreach($tipo->profesion as $prof)
		<hr>
		<p> &emsp;
 		
 		<div>
 			<label for="profesional"> -Profesional</label>
 			<a id="profesional" style="text-decoration: none;color: blue" href="{{route('contatacion.profesional.perfil',$prof->profesional->id)}}">  {{ $prof->profesional->usuario->nick }} </a>
 		</div>
			   

			@if(isset($prof->profesional->usuario->perfil_usuario ))
				-Nombre Completo: {{ $prof->profesional->usuario->perfil_usuario->nombre_completo }}
			@else
				
			@endif
			@if($prof->servicios()->get()->isEmpty())
				<p>No tiene servicio registrado</p>
			@else
			@foreach($prof->servicios as $servicio)

			<p>&emsp; &emsp;
				{{ $servicio->nombre }}&emsp;
				Descripcion: {{ $servicio->descripcion }}
				&emsp;Ciudad:{{ $servicio->ubicacion_servicio->ciudad }}
				&emsp; Cotizado:

					@if($servicio->es_contizado)
						 <span class="glyphicon glyphicon-ok"></span>
						 &emsp;Precio: {{ $servicio->precio }}
                      
					@else
						 <span class="glyphicon glyphicon-remove"></span>


					@endif
				&emsp;
				<a style="text-decoration: none;color: blue"  href="{{ route('contratacion.index',$servicio->id)}}">Contratar Servicio</a>

			</p>	


				
			@endforeach
			@endif
		</p>
	@endforeach

	<hr>

@endforeach

@endif
</div>