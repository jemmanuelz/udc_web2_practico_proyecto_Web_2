	
    <div class="form-group row">
        {!! Form::label('cui', 'Cuil:'.(isset($read)? '' : '*'),['class'=>'col-sm-1 form-control-label']) !!}
            
            <div class="col-sm-6">
				{!! Form::text('cuil',isset($usuario->profesional)? $usuario->profesional->cuil:null,['class'=>'form-control','id' => 'cuil' , (isset($read)? 'readonly': '') ]) !!}
            </div>
    </div>

    <div class="form-group row">
        
            {!! Form::label('telefono_alternativo', 'Telefono Aternativo:'.(isset($read)? '' : '*'),['class'=>'col-sm-1 form-control-label']) !!}
        
            <div class="col-sm-6">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-phone fa-fw w3-margin-right w3-large w3-text-teal"></i></span>
                
                    {!! Form::text('telefono_alternativo',isset($usuario->profesional)? $usuario->profesional->telefono_alternativo:null,['class'=>'form-control','id' => 'telefono_alternativo',(isset($read)? 'readonly': '')  ]) !!}
                </div>                   
            </div>
    </div>




{{--
@section('script')
<script type="text/javascript">
    
$(function() {


    $("#telefono_alternativo").inputmask({
            mask: "0+(999)4-999999"
        });
  $("#cuil").inputmask({
            mask: "99-99999999-9"
        });


});



</script>

@endsection
 --}}