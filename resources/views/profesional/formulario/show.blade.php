@extends('layouts.template')

@section('htmlheader_title')
    Usuario|{{$usuario->nick}}|Profesional|Ver
@endsection


@section('contentheader_title')
   Usuario: {{$usuario->nick}}
@endsection

@section('contentheader_description') 
    Profesional|datos 
@endsection
       



@section('main-content')


    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active">
                 <a aria-expanded="false" href="#tab_3" data-toggle="tab"><i class="fa fa-briefcase fa-fw w3-margin-right w3-large w3-text-teal"></i>Datos Laborales</a>
            </li>
        </ul>
        <div class="tab-content">
            @include('profesional.partials.form')
        </div>


        <div class="box-footer">
        
             @if(!(isset($read)))
                {!! Form::submit('Guardar', ['class' => 'btn btn-success ','id' => 'guardar']) !!}
                 <a href="{{ url('/') }}" class="btn btn-danger btn">cancelar</a>
            @else
                  <a href="{{route('usuario.profesional.edit',$usuario->profesional->id)}}" class="btn btn-warning ">Editar</a>

              
                  <a href="{{ route('usuario.datos') }}" class="btn btn-danger btn">volver</a>

            @endif


         </div>


    </div>
             


@endsection




