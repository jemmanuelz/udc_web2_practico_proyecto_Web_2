@extends('layouts.template')

@section('htmlheader_title')
    Usuario|{{$usuario->nick}}|Profesional|Nuevo
@endsection


@section('contentheader_title')
   Usuario: {{$usuario->nick}}
@endsection

@section('contentheader_description') 
    Profesional|Nuevo
@endsection
       



@section('main-content')

 {!! Form::model($usuario,['route'=>['usuario.profesional.store',$usuario->id],'id'=>'usuario_profesional']) !!}

	<div class="nav-tabs-custom">
		<ul class="nav nav-tabs">
            <li class="active">
                 <a aria-expanded="false" href="#tab_3" data-toggle="tab"><i class="fa fa-briefcase fa-fw w3-margin-right w3-large w3-text-teal"></i>Datos Laborales</a>
            </li>
        </ul>
		<div class="tab-content">

   
        	@include('profesional.partials.form')
        
 

 		</div>


    	<div class="box-footer">
        
     		 
                {!! Form::submit('Guardar', ['class' => 'btn btn-success ','id' => 'guardar']) !!}
                 <a href="{{ route('usuario.datos' ) }}" class="btn btn-danger btn">volver</a>         

         </div>


    </div>
             
   {!! Form::close() !!}

@endsection