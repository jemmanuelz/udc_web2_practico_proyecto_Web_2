$(function () {

			$("ul.sub a").on('click',function (e){
				e.preventDefault();
				
				$('#vista_categorias').load(this.href);
			
			});


	  $('#tabla_servicios').DataTable({
           // "serverSide":true,
            "info":     true,
            "language": {
 
              "sProcessing": "Procesando...", 
              "sLengthMenu": "Mostrar _MENU_ registros",
              "sZeroRecords": "No se encontraron resultados",
              "sEmptyTable": "Ningún dato disponible en esta tabla",
              "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
              "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
              "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
              "sInfoPostFix": "",
              "sSearch": "Buscar:",
              "sUrl": "",
              "sInfoThousands": ",",
              "sLoadingRecords": "Cargando...",
              "oPaginate": {
                  "sFirst": "Primero",
                  "sLast": "Último",
                  "sNext": "Siguiente",
                  "sPrevious": "Anterior"
 
                },
                "oAria": {
                    "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
 
                }
 
          },

        //order: [[2, 'asc']],
                    
                    initComplete: function() {
                        var api = this.api();
                        var select = $('#servicioSelect');
                        api.column(0).data().unique().sort().each( function ( d, j ) {
                            select.append( '<option value="'+d+'">'+d+'</option>' )
                        } );   
                  
                        var select = $('#profesionSelect');
                        api.column(1).data().unique().sort().each( function ( d, j ) {
                            select.append( '<option value="'+d+'">'+d+'</option>' )
                        } );   
                  
                        var select = $('#tipo_servicioSelect');
                        api.column(2).data().unique().sort().each( function ( d, j ) {
                            select.append( '<option value="'+d+'">'+d+'</option>' )
                        } );   
                    }


                });

                $('#servicioSelect').change(function() {
                    var val = $.fn.dataTable.util.escapeRegex(
                        $(this).val()
                    );
                    $('#tabla_servicios').column(0)
                        .search( val == 'All' ? '' : '^'+val+'$', true, false )
                        .draw();
                });

                  $('#profesionSelect').change(function() {
                    var val = $.fn.dataTable.util.escapeRegex(
                        $(this).val()
                    );
                    $('#tabla_servicios').column(1)
                        .search( val == 'All' ? '' : '^'+val+'$', true, false )
                        .draw();
                });

                $('#tipo_servicioSelect').change(function() {
                    var val = $.fn.dataTable.util.escapeRegex(
                        $(this).val()
                    );
                    $('#tabla_servicios').column(2)
                        .search( val == 'All' ? '' : '^'+val+'$', true, false )
                        .draw();
                });




        $('#es_contizado').on('click', function(e){
        
        //var nro_ticket = e.target.value;
             
        var bol= $('#es_contizado').prop('checked');


        if (  bol ) {
            $('#tick_es_cotizado').show('fast');                    
        }else{
			$('#tick_es_cotizado').hide('fast');
            $('#precio').val(0);
                    
        }          
                    
     });


     $("#like").on("click",function (e){
        e.preventDefault();
        
        $("#likes").load(this.href);
      
      });

       $("#dislike").on("click",function (e){
        e.preventDefault();
        
        $("#likes").load(this.href);
      
      });

    $("#telefono_alternativo").inputmask({
            mask: "0+(999)4-999999"
    });
  	$("#cuil").inputmask({
            mask: "99-99999999-9"
    });

      $("#telefono").inputmask({
            mask: "+54(999)999-9999"
        });
















 	 $('#tabla_profesiones').DataTable({
           
            "info":     true,
            "language": {
 
              "sProcessing": "Procesando...", 
              "sLengthMenu": "Mostrar _MENU_ registros",
              "sZeroRecords": "No se encontraron resultados",
              "sEmptyTable": "Ningún dato disponible en esta tabla",
              "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
              "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
              "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
              "sInfoPostFix": "",
              "sSearch": "Buscar:",
              "sUrl": "",
              "sInfoThousands": ",",
              "sLoadingRecords": "Cargando...",
              "oPaginate": {
                  "sFirst": "Primero",
                  "sLast": "Último",
                  "sNext": "Siguiente",
                  "sPrevious": "Anterior"
 
                },
                "oAria": {
                    "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
 
                }
 
          },

       
                    
                    initComplete: function() {
                        var api = this.api();
                        var select = $('#profesionSelect');
                        api.column(0).data().unique().sort().each( function ( d, j ) {
                            select.append( '<option value="'+d+'">'+d+'</option>' )
                        } );   
                    }
          });

                $('#profesionSelect').change(function() {
                    var val = $.fn.dataTable.util.escapeRegex(
                        $(this).val()
                    );
                    $('#tabla_profesiones').column(0)
                        .search( val == 'All' ? '' : '^'+val+'$', true, false )
                        .draw();
                });



    



});
