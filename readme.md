## Descripcion 

 # Materia: Web 2
 
 # Año: 2016
 
 # Alumno: Zambrano Emmanuel
 

 
     Proyecto que simula un web_app donde se ofrece al comunidad un lugar donde puede ofertar y/ prestar su servicio entre usuarios,
     donde un usuario en un momento critico o cotidiano ( Ej: levantar una habitacion nueva en un lugar, animar una fiesta infantil, instalar plomeria nueva,etc ) 
     y se necesite a un profecional/es,dicho usuario pueda contactarlo de manera online, pedir su presupuesto/s,eligiendo lo por diversos criterios tales como 
    por su calidad,precio,profesional.
    
    
    
    #Requerimientos funcionales:
    
    -ABM de usuarios y permisos
        *auto-registro de usuarios via pagina (ej: confirmacion de email)
    
    -Login usuario:
    
        *Quien solicita un servicio.
        *Datos necesarios 
            -teléfono
            -domicilio
            -Nombre completo.
            
    -Login profesional:
    
        *Quien se registra y crea un servicio de una categoria y/o en una subcategoria
        *Datos necesarios: 
            -datos personales como teléfono
            -nombre completo 
            -rubros que abarca el servicio/s que ofrezca.
    
    -CRUD de servicio de un profesional,este puede tener:
    
        *Descripción
        *Opinión (la opción de dejar una opinión personal del servicio)
        *Reputación
        *Ubicación
        *Cobertura en que se ofrece.
        *Nombre del oferente ,con la opción de escribir una consulta directa  más la posibilidad de adjuntar un archivo (opcional)
        *Zona de cobertura(Rawson,Trelew) .
        *Cotización ( con precio/a cotizar )
        *Tipo de servicio ( en su lugar/ a domicilio )
            
    -CRUD de categoria de un servicio:
    
        -CRUD de subcategoria
        *Categoría principal
            -Subcategoría
            -servicio (este puede tener precio o puede ser consultable)
            -filtro por ciudad o zona de cobertura,tipo de servicio,cotizacion.
            
            
    -Registro de puntuacion de la comunidad de un profesional determinado en un servicio determinado
    -Publicaciones destacadas : El oferente pueda publicitar por demás en un espacio del sitio algún proyecto/servicio (descripción con imagen) durante unos días.
    -Gestionar proyecto/servicio.(historial).
    
    
    
    #Forma de operación:
    
        -Registro a través de cuenta de facebook o google.(opcional)
        -Tener un perfil del demandante ( cliente ).Datos necesarios teléfono,domicilio,Nombre completo.
        -Tener un perfil del oferente ( profesionales ),con puntuación de calidad,profesionalidad y responsabilidad.
        -El demandante pueda elegir rubros de servicio que necesite y envíe su consulta (pedido)
        -El oferente reciba la consulta y decida contestar al pedido ,enviando presupuesto.
        -El demandante reciba los presupuesto de su consulta y elija el oferente adecuado a su necesidad,precio u otro factor .
        -El demandante contacte por dicho medio al oferente y lo puntúe por el servicio.

    
    

# Laravel PHP Framework

[![Build Status](https://travis-ci.org/laravel/framework.svg)](https://travis-ci.org/laravel/framework)
[![Total Downloads](https://poser.pugx.org/laravel/framework/d/total.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/framework/v/stable.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/framework/v/unstable.svg)](https://packagist.org/packages/laravel/framework)
[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as authentication, routing, sessions, queueing, and caching.

Laravel is accessible, yet powerful, providing tools needed for large, robust applications. A superb inversion of control container, expressive migration system, and tightly integrated unit testing support give you the tools you need to build any application with which you are tasked.

## Official Documentation

Documentation for the framework can be found on the [Laravel website](http://laravel.com/docs).

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](http://laravel.com/docs/contributions).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell at taylor@laravel.com. All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).


