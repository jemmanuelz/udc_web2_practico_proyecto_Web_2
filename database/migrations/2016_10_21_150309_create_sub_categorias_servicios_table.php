<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubCategoriasServiciosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_categorias_servicios', function (Blueprint $table) {
            
            $table
                ->increments('id');

            $table
                ->string('valor')
                ->index();

            $table
                ->boolean('activo')
                 ->default(true);

            $table
               ->integer('categoria_servicio_id')
                    ->unsigned()
                    ->index();

            $table->foreign('categoria_servicio_id')
                    ->references('id')
                    ->on('categorias_servicios');

            $table
                ->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sub_categorias_servicios');
    }
}
