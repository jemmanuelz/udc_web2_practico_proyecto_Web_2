<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContratacionesServiciosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contrataciones_servicios', function (Blueprint $table) {
            
            $table
                ->increments('id');
            $table
                ->double('puntuacion_servicio',2,2);

            $table
                ->double('precio');
            
            $table
                ->date('fecha');

            $table
               ->integer('user_id')
                    ->unsigned()
                    ->index();

            $table->foreign('user_id')
                    ->references('id')
                    ->on('users');

            $table
               ->integer('servicio_id')
                    ->unsigned()
                    ->index();

            $table->foreign('servicio_id')
                    ->references('id')
                    ->on('servicios');


            $table
               ->integer('estado_contratacion_id')
                    ->unsigned()
                    ->index();

            $table->foreign('estado_contratacion_id')
                    ->references('id')
                    ->on('estados_contrataciones');
            


            $table
                ->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('contrataciones_servicios');
    }
}
