<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePerfilesUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('perfiles_usuarios', function (Blueprint $table) {
            
            $table
                ->increments('id');

            $table
                ->boolean('activo')
                    ->default(true);
            $table
                ->char('telefono',15);
           
            $table
                ->string('domicilio',50);

            $table
                ->string('nombre_completo',50);

            $table
               ->integer('user_id')
                    ->unsigned()
                    ->index();

            $table->foreign('user_id')
                    ->references('id')
                    ->on('users');


            $table
                ->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('perfiles_usuarios');
    }
}
