<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComentariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comentarios', function (Blueprint $table) {
            
            $table
                ->increments('id');

            $table
                ->datetime('fecha');

            $table
                ->string('descripcion');
            
            $table
               ->integer('user_id')
                    ->unsigned()
                     ->nullable()
                        ->default(null)
                        ->index();

            $table->foreign('user_id')
                    ->references('id')
                    ->on('users');

            $table
               ->integer('servicio_id')
                    ->unsigned()
                    ->index();

            $table->foreign('servicio_id')
                    ->references('id')
                    ->on('servicios');
            
        


            $table
                ->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('comentarios');
    }
}
