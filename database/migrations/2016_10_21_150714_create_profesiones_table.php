<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfesionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profesiones', function (Blueprint $table) {
            
            $table
                ->increments('id');

            $table
                ->smallInteger('experiencia');
              $table
                ->boolean('activo')
                    ->default(true);

            $table
               ->integer('profesional_id')
                    ->unsigned()
                    ->nullable()
                        ->default(null)
                        ->index();

            $table->foreign('profesional_id')
                    ->references('id')
                    ->on('profesionales');



            $table
               ->integer('tipo_profesion_id')
                    ->unsigned()
                    ->nullable()
                        ->default(null)
                        ->index();

            $table->foreign('tipo_profesion_id')
                    ->references('id')
                    ->on('tipo_profesiones');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('profesiones');
    }
}
