<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            
            $table
                ->increments('id');
            $table
                ->string('nick',20);
            $table
                ->string('email',30)->unique();
            $table
                ->string('password',10);
            
            $table
                ->boolean('activo')
                    ->default(true);

            $table
                ->rememberToken();
            $table
                ->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
