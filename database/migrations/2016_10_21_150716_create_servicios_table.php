<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiciosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('servicios', function (Blueprint $table) {

            $table
                ->increments('id');
            $table
                ->boolean('es_contizado')
                    ->default(false);

            $table
                ->double('precio')->default(0.0);

            $table
                ->boolean('activo')
                    ->default(true);

            $table
                ->string('nombre',100);

            $table
                ->string('descripcion',200)
                ->default('no tiene descripcion alguna');

            $table
                ->smallInteger('like')
                ->defaul(0);
            $table
                ->smallInteger('dislike')
                ->defaul(0);


            $table
               ->integer('ubicacion_servicio_id')
                    ->unsigned()
                    ->nullable()
                        ->default(null)
                        ->index();

            $table->foreign('ubicacion_servicio_id')
                    ->references('id')
                    ->on('ubicacion_servicios');

            $table
               ->integer('tipo_servicio_id') //en lugar o domicilio
                    ->unsigned()
                    ->nullable()
                        ->default(null)
                         ->index();

            $table->foreign('tipo_servicio_id')
                    ->references('id')
                    ->on('tipos_servicios');

            $table
               ->integer('profesion_id')
                    ->unsigned()
                     ->index();

            $table->foreign('profesion_id') 
                    ->references('id')
                    ->on('profesiones');



            $table
                ->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('servicios');
    }
}
