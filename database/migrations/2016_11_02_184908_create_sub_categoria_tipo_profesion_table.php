<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubCategoriaTipoProfesionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_categoria_tipo_profesion', function (Blueprint $table) {
            
            $table
                ->increments('id');

            $table
               ->integer('sub_categoria_servicio_id')
                    ->unsigned()
                    ->nullable()                    
                        ->default(null)
                        ->index();

            $table->foreign('sub_categoria_servicio_id')
                    ->references('id')
                    ->on('sub_categorias_servicios');

             $table
               ->integer('tipo_profesion_id')
                    ->unsigned()
                    ->nullable()                    
                        ->default(null)
                        ->index();

            $table->foreign('tipo_profesion_id')
                    ->references('id')
                    ->on('tipo_profesiones');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sub_categoria_tipo_profesion');
    }
}
