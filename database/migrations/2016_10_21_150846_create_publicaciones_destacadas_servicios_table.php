<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePublicacionesDestacadasServiciosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('publicaciones_destacadas_servicios', function (Blueprint $table) {
           

            $table
                ->increments('id');
            $table
                ->boolean('activo')
                 ->default(true);

             $table
                ->string('descripcion');

            $table
                ->date('fecha');

            $table
               ->integer('profesional_id')
                    ->unsigned()
                    ->nullable()
                        ->default(null)
                        ->index();

            $table->foreign('profesional_id')
                    ->references('id')
                    ->on('profesionales');


            $table
               ->integer('servicio_id')
                    ->unsigned()
                    ->nullable()
                        ->default(null)
                        ->index();

            $table->foreign('servicio_id')
                    ->references('id')
                    ->on('servicios');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('publicaciones_destacadas_servicios');
    }
}
