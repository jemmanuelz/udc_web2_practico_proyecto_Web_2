<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUbicacionServiciosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ubicacion_servicios', function (Blueprint $table) {

            $table
                ->increments('id');
                //->primary();

            $table
                ->string('ciudad',50)
                ->index();

            $table
                ->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ubicacion_servicios');

       // $table->dropIndex('geo_state_index');
       // $table->dropPrimary('users_id_primary');
    }
}
