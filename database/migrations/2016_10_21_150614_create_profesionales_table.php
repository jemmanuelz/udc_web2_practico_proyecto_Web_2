<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfesionalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profesionales', function (Blueprint $table) {
            
            $table
                ->increments('id');
            $table
                ->char('cuil',8);
            $table
                ->char('telefono_alternativo',15);
            $table
                ->boolean('activo')
                    ->default(true);

            $table
               ->integer('user_id')
                    ->unsigned()
                    ->index();

            $table->foreign('user_id')
                    ->references('id')
                    ->on('users');
            
            $table
                ->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('profesionales');
    }
}
