<?php

use Illuminate\Database\Seeder;
use App\Tipo_Profesion as Profesion;
use App\Sub_Categoria_Servicio as Sub;


class TipoProfesionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
          \DB::table('tipo_profesiones')->truncate();

         $mantenimiento=Sub::determinadaObj( 'Mantenimiento' );

         $albañileria=Sub::determinadaObj( 'Albañilería' );
         $construcción_en_seco=Sub::determinadaObj( 'Construcción_en_Seco' ); 






         $p=Profesion::create(['nombre' => 'albañil' ]);
         $p->sub_categoria_servicios()->save($albañileria);
         $p->sub_categoria_servicios()->save($construcción_en_seco);
         $p->sub_categoria_servicios()->save($mantenimiento);
         $p->save();
        

         $carpinteria=Sub::determinadaObj( 'Carpintería' );
         $p=Profesion::create(['nombre' => 'carpintero' ]);
         $p->sub_categoria_servicios()->save($carpinteria);
         $p->sub_categoria_servicios()->save($mantenimiento);
        



         $plomeria=Sub::determinadaObj( 'Plomeria' );
         $p=Profesion::create(['nombre' => 'plomero' ]);
         $p->sub_categoria_servicios()->save($plomeria);
         $p->sub_categoria_servicios()->save($mantenimiento);
       


 		 $gasistas=Sub::determinadaObj( 'Gasistas' );
 		 $p=Profesion::create(['nombre' => 'gasista' ]);
         $p->sub_categoria_servicios()->save($gasistas);
 		 $p->sub_categoria_servicios()->save($mantenimiento);
 		 

 		 $entretenimiento=Sub::determinadaObj( 'Entretenimiento' );

 		 $infantiles=Sub::determinadaObj( 'Shows_infantiles' );



 		 $p=Profesion::create(['nombre' => 'mago' ]);
         $p->sub_categoria_servicios()->save($infantiles);
 		 $p->sub_categoria_servicios()->save($entretenimiento);
 	
 		 

 		 $dj=Sub::determinadaObj( 'DJs,_Sonido_e_Iluminación' );
 		 $p=Profesion::create(['nombre' => 'dj' ]);

         $p->sub_categoria_servicios()->save($dj);
 		 $p->sub_categoria_servicios()->save($entretenimiento);
 		 $p->sub_categoria_servicios()->save($infantiles);
 		

       
              
       	$minifletes=Sub::determinadaObj( 'Minifletes' );
       	$fletes=Sub::determinadaObj( 'Fletes' );
       	$camion_grande=Sub::determinadaObj( 'Camión_Grande' ); 

        $p=Profesion::create(['nombre' => 'fletero' ]);
        $p->sub_categoria_servicios()->save($camion_grande );
        $p->sub_categoria_servicios()->save($fletes);
 		$p->sub_categoria_servicios()->save($minifletes);



    }
}



 		