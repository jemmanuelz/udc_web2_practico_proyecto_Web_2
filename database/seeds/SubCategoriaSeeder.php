<?php

use Illuminate\Database\Seeder;
use App\Categoria_Servicio as Categoria;
use App\Sub_Categoria_Servicio as Sub;
use App\Ubicacion_Servicio as Ubicacion;

class SubCategoriaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         \DB::table('sub_categorias_servicios')->truncate();

         $categoria=Categoria::determinada( 'Hogar' );
         Sub::create(['categoria_servicio_id' => $categoria,'valor' => 'Albañilería']);
         Sub::create(['categoria_servicio_id' => $categoria,'valor' => 'Carpintería']);
         Sub::create(['categoria_servicio_id' => $categoria,'valor' => 'Construcción_en_Seco']);
         Sub::create(['categoria_servicio_id' => $categoria,'valor' => 'Electricista']);
         Sub::create(['categoria_servicio_id' => $categoria,'valor' => 'Plomeria']);
         Sub::create(['categoria_servicio_id' => $categoria,'valor' => 'Mantenimiento']);
         Sub::create(['categoria_servicio_id' => $categoria,'valor' => 'Gasistas']);


         $categoria=Categoria::determinada( 'Fiesta' );
         Sub::create(['categoria_servicio_id' => $categoria, 'valor' => 'Karaoke']);
         Sub::create(['categoria_servicio_id' => $categoria, 'valor' => 'Catering']);
         Sub::create(['categoria_servicio_id' => $categoria, 'valor' => 'DJs,_Sonido_e_Iluminación']);
         Sub::create(['categoria_servicio_id' => $categoria, 'valor' => 'Shows_infantiles']);
         Sub::create(['categoria_servicio_id' => $categoria, 'valor' => 'Entretenimiento']);

         $categoria=Categoria::determinada( 'Transporte' );
         Sub::create(['categoria_servicio_id' => $categoria, 'valor' => 'Motomensajerías']);
         Sub::create(['categoria_servicio_id' => $categoria, 'valor' => 'Minifletes']);
         Sub::create(['categoria_servicio_id' => $categoria, 'valor' => 'Fletes']);
         Sub::create(['categoria_servicio_id' => $categoria, 'valor' => 'Camión_Grande']);

         $categoria=Categoria::determinada( 'Curso' );
         Sub::create(['categoria_servicio_id' => $categoria, 'valor' => 'Cocina']);
         Sub::create(['categoria_servicio_id' => $categoria, 'valor' => 'Computación']);
         Sub::create(['categoria_servicio_id' => $categoria, 'valor' => 'Deportes']);
         Sub::create(['categoria_servicio_id' => $categoria, 'valor' => 'Manejo']);
         
         $categoria=Categoria::determinada( 'Otro' );
         Sub::create(['categoria_servicio_id' => $categoria, 'valor' => 'Servicios_para_Mascotas']);
         Sub::create(['categoria_servicio_id' => $categoria, 'valor' => 'Servicios_para_Oficinas']);
         Sub::create(['categoria_servicio_id' => $categoria, 'valor' => 'Viajes_y_Turismo']);
         Sub::create(['categoria_servicio_id' => $categoria, 'valor' => 'Regalos_personalizados']);


        $categoria=Categoria::determinada( 'Cotizable' );
         Sub::create(['categoria_servicio_id' => $categoria, 'valor' => 'Cotizacion']);
         Sub::create(['categoria_servicio_id' => $categoria, 'valor' => 'A_consultar']);

        


    }
}
