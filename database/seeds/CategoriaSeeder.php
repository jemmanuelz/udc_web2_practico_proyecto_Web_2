<?php

use Illuminate\Database\Seeder;
use App\Categoria_Servicio as Categoria;


class CategoriaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         \DB::table('categorias_servicios')->truncate();

         Categoria::create(['valor' => 'Hogar']);
         Categoria::create(['valor' => 'Fiesta']);
         Categoria::create(['valor' => 'Transporte']);
         Categoria::create(['valor' => 'Curso']);
         Categoria::create(['valor' => 'Otro']);
         Categoria::create(['valor' => 'Cotizable']);

         
    }
}
