<?php

use Illuminate\Database\Seeder;
use App\Estado_Contratacion as Estado;

class EstadoContratacionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('estados_contrataciones')->truncate();

        Estado::create(['valor' => 'aceptada']);
        Estado::create(['valor' => 'cancelada']);
        Estado::create(['valor' => 'pendiente']);
    }
}
