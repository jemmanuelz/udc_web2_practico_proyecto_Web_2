<?php

use Illuminate\Database\Seeder;
use App\Ubicacion_Servicio as Ubicacion;
class UbicacionServicioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         \DB::table('ubicacion_servicios')->truncate();

         Ubicacion::create(['ciudad' => 'Rawson']);
         Ubicacion::create(['ciudad' => 'Trelew']);
         Ubicacion::create(['ciudad' => 'Pto.Madrin']);
         Ubicacion::create(['ciudad' => 'Gaiman']);
         Ubicacion::create(['ciudad' => 'Dolavon']);
         Ubicacion::create(['ciudad' => 'Comodoro Rivadavia']);
         Ubicacion::create(['ciudad' => 'GanGan']);



    }
}
