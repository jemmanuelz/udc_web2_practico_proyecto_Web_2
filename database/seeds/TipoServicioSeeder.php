<?php

use Illuminate\Database\Seeder;
use  App\Tipo_Servicio as Tipo;

class TipoServicioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         \DB::table('tipos_servicios')->truncate();

         Tipo::create(['valor' => 'Domicio']);
         Tipo::create(['valor' => 'Distancia']);
    }
}
