<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::statement('SET FOREIGN_KEY_CHECKS=0;'); 
        Model::unguard();

        	$this->call(UbicacionServicioSeeder::class);
        	$this->call(CategoriaSeeder::class);
        	$this->call(SubCategoriaSeeder::class);
            $this->call(TipoProfesionSeeder::class);
            $this->call(TipoServicioSeeder::class);
            $this->call(EstadoContratacionSeeder::class);

       Model::reguard();
       DB::statement('SET FOREIGN_KEY_CHECKS=1;');

    }
}
